//
//  bitset.c
//  NLTextEncoder
//
//  Created by yussairi on 16/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "bitset.h"

void set_char_bit(unsigned int bit_index, unsigned char* number)
{
    *number |= 1UL << bit_index;
}

void set_int_bit(unsigned int bit_index, unsigned int* number)
{
    *number |= 1UL << bit_index;
}

void clear_char_bit(unsigned int bit_index, unsigned char* number)
{
    *number &= ~(1UL << bit_index);
}

void clear_int_bit(unsigned int bit_index, unsigned int* number)
{
    *number &= ~(1UL << bit_index);
}

int get_char_bit_value(unsigned int bit_index, unsigned char number)
{
    return (number >> bit_index) & 1U;
}

int get_int_bit_value(unsigned int bit_index, unsigned int number)
{
    return (number >> bit_index) & 1U;
}

void split_int_to_chars(unsigned int number, unsigned char* bytes)
{
    //unsigned char bytes[4];
    //unsigned long n = 175;
    
    bytes[0] = (number >> 24) & 0xFF;
    bytes[1] = (number >> 16) & 0xFF;
    bytes[2] = (number >> 8) & 0xFF;
    bytes[3] = number & 0xFF;
}

void split_uint64_to_chars(unsigned long long number, unsigned char* bytes)
{
    //unsigned char bytes[4];
    //unsigned long n = 175;
    bytes[0] = (number >> 56) & 0xFF;
    bytes[1] = (number >> 48) & 0xFF;
    bytes[2] = (number >> 40) & 0xFF;
    bytes[3] = (number >> 32) & 0xFF;
    bytes[4] = (number >> 24) & 0xFF;
    bytes[5] = (number >> 16) & 0xFF;
    bytes[6] = (number >> 8) & 0xFF;
    bytes[7] = number & 0xFF;
}

void split_uint64_to_uint(unsigned long long number, unsigned int* ints)
{
    ints[0] = (number >> 48) & 0xFF;
    ints[1] = (number >> 32) & 0xFF;
    ints[2] = (number >> 16) & 0xFF;
    ints[3] = number & 0xFF;
}


void convert_char_to_bit_array(unsigned char byte, int* bits)
{
    for (int i=0; i < 8; ++i)
    {
        bits[i]=get_char_bit_value(i, byte);
    }
}
