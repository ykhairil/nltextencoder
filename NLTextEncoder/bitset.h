//
//  bitset.h
//  NLTextEncoder
//
//  Created by yussairi on 16/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef bitset_h
#define bitset_h

#include <stdio.h>

void set_char_bit(unsigned int bit_index, unsigned char* number);
void clear_char_bit(unsigned int bit_index, unsigned char* number);
void set_int_bit(unsigned int bit_index, unsigned int* number);
void clear_int_bit(unsigned int bit_index, unsigned int* number);
int get_char_bit_value(unsigned int bit_index, unsigned char number);
int get_int_bit_value(unsigned int bit_index, unsigned int number);
void split_int_to_chars(unsigned int number, unsigned char* bytes);
void convert_char_to_bit_array(unsigned char byte, int* bits);
void split_uint64_to_chars(unsigned long long number, unsigned char* bytes);
void split_uint64_to_uint(unsigned long long number, unsigned int* ints);
#endif /* bitset_h */
