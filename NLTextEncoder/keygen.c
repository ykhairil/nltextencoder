//
//  keygen.c
//  NLTextEncoder
//
//  Created by yussairi on 01/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//
//  Create keying function for use in the compression
//  algorithm

#include "keygen.h"
#include <stdlib.h>
#include <math.h>
#include "bitset.h"

#define ROTL8(x,shift) ((uint8_t) ((x) << (shift)) | ((x) >> (8 - (shift))))

//the first 200 prime numbers
//as examples for keygen
//actual implementation should use
//bigger primes
unsigned long primes_size = 300;
unsigned long primes[300]=
{
    2,
    3,
    5,
    7,
    11,
    13,
    17,
    19,
    23,
    29,
    31,
    37,
    41,
    43,
    47,
    53,
    59,
    61,
    67,
    71,
    73,
    79,
    83,
    89,
    97,
    101,
    103,
    107,
    109,
    113,
    127,
    131,
    137,
    139,
    149,
    151,
    157,
    163,
    167,
    173,
    179,
    181,
    191,
    193,
    197,
    199,
    211,
    223,
    227,
    229,
    233,
    239,
    241,
    251,
    257,
    263,
    269,
    271,
    277,
    281,
    283,
    293,
    307,
    311,
    313,
    317,
    331,
    337,
    347,
    349,
    353,
    359,
    367,
    373,
    379,
    383,
    389,
    397,
    401,
    409,
    419,
    421,
    431,
    433,
    439,
    443,
    449,
    457,
    461,
    463,
    467,
    479,
    487,
    491,
    499,
    503,
    509,
    521,
    523,
    541,
    547,
    557,
    563,
    569,
    571,
    577,
    587,
    593,
    599,
    601,
    607,
    613,
    617,
    619,
    631,
    641,
    643,
    647,
    653,
    659,
    661,
    673,
    677,
    683,
    691,
    701,
    709,
    719,
    727,
    733,
    739,
    743,
    751,
    757,
    761,
    769,
    773,
    787,
    797,
    809,
    811,
    821,
    823,
    827,
    829,
    839,
    853,
    857,
    859,
    863,
    877,
    881,
    883,
    887,
    907,
    911,
    919,
    929,
    937,
    941,
    947,
    953,
    967,
    971,
    977,
    983,
    991,
    997,
    1009,
    1013,
    1019,
    1021,
    1031,
    1033,
    1039,
    1049,
    1051,
    1061,
    1063,
    1069,
    1087,
    1091,
    1093,
    1097,
    1103,
    1109,
    1117,
    1123,
    1129,
    1151,
    1153,
    1163,
    1171,
    1181,
    1187,
    1193,
    1201,
    1213,
    1217,
    1223,
    1229,
    1231,
    1237,
    1249,
    1259,
    1277,
    1279,
    1283,
    1289,
    1291,
    1297,
    1301,
    1303,
    1307,
    1319,
    1321,
    1327,
    1361,
    1367,
    1373,
    1381,
    1399,
    1409,
    1423,
    1427,
    1429,
    1433,
    1439,
    1447,
    1451,
    1453,
    1459,
    1471,
    1481,
    1483,
    1487,
    1489,
    1493,
    1499,
    1511,
    1523,
    1531,
    1543,
    1549,
    1553,
    1559,
    1567,
    1571,
    1579,
    1583,
    1597,
    1601,
    1607,
    1609,
    1613,
    1619,
    1621,
    1627,
    1637,
    1657,
    1663,
    1667,
    1669,
    1693,
    1697,
    1699,
    1709,
    1721,
    1723,
    1733,
    1741,
    1747,
    1753,
    1759,
    1777,
    1783,
    1787,
    1789,
    1801,
    1811,
    1823,
    1831,
    1847,
    1861,
    1867,
    1871,
    1873,
    1877,
    1879,
    1889,
    1901,
    1907,
    1913,
    1931,
    1933,
    1949,
    1951,
    1973,
    1979,
    1987
};

unsigned long long max_keyspace = 0;

unsigned long long max_prime = 0xffffffffffffffc5ull;

//Diffie-hellman functions

unsigned long long get_biggest_prime_base()
{
    return max_prime;
}

unsigned long long get_prime_base(int index)
{
    if (index >= 0 && index < (int)primes_size)
        return primes[index];
    else
        return 0;
}

unsigned long long factorial(unsigned long number)
{
    if (number == 0)
        return 1;
    else
        return (number * factorial(number - 1));
}

//calc a*b mod P
unsigned long long mul_mod_p(unsigned long long a, unsigned long long b, unsigned long long P)
{
    unsigned long long m = 0;
    while(b) {
        if(b&1) {
            unsigned long long t = P-a;
            if ( m >= t) {
                m -= t;
            } else {
                m += a;
            }
        }
        if (a >= P - a) {
            a = a * 2 - P;
        } else {
            a = a * 2;
        }
        b>>=1;
    }
    return m;
}

unsigned long long pow_mod_p(unsigned long long a, unsigned long long b, unsigned long long P)
{
    if (b==1) {
        return a;
    }
    unsigned long long t = pow_mod_p(a, b>>1,P);
    t = mul_mod_p(t,t, P);
    if (b % 2) {
        t = mul_mod_p(t, a,P);
    }
    return t;
}

// calc a^b mod P
unsigned long long powmodp(unsigned long long a, unsigned long long b, unsigned long long P)
{
    if (a > P)
        a%=P;
    return pow_mod_p(a,b,P);
}

void get_diffie_hellman_64bit_public_key(unsigned long long prime_base, unsigned long long prime_modulus, unsigned long long secret_a, unsigned long long secret_b, unsigned long long* public_key_A, unsigned long long* public_key_B)
{
    *public_key_A = powmodp(prime_modulus, secret_a, prime_base);
    *public_key_B = powmodp(prime_modulus, secret_b, prime_base);
}

unsigned long long get_diffie_hellman_64bit_private_key(unsigned long long public_key, unsigned long long secret_key, unsigned long long prime_base)
{
    return powmodp(public_key, secret_key, prime_base);
}

//Heap's algorithm function for permutations generation
//in case algorithmic key-based permutation is unreliable

void swap (int *x, int *y)
{
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void print_array(const int *v, int len)
{
    int i;
    int size = len;
    if (v != 0) {
        for ( i = 0; i < size; i++) {
            printf("%4d", v[i] );
        }
        printf("\n");
    }
}

void heap_permute(int* v, int n) {
    int i;
    if (n == 1) {
        print_array(v, n);
    }
    else {
        for (i = 0; i < n; i++) {
            heap_permute(v, n-1);
            if (n % 2 == 1) {
                swap(&v[0], &v[n-1]);
            }
            else {
                swap(&v[i], &v[n-1]);
            }
        }
    }
}

int compare (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

//2KDP algorithm for creating permutation of number series
//based on two numbers as keys
//avoiding need to create every permutation beforehand
//http://dx.doi.org/10.1155/2014/795292
void generate_2kdp_permutation(int array_size, int* output_array, unsigned long long key_1, unsigned long long key_2)
{
	unsigned long long p = 0;
	unsigned long long w = 0;
	unsigned long long x = 0;
	unsigned long long y = 0;
	unsigned long long z = 0;
	unsigned long duplicate_k = 0;
    int* set_i = NULL;
	int* set_c = NULL;
    int* set_r = NULL;
	int* set_u = NULL;
    int* set_m = NULL;
    
    set_i = calloc(array_size, sizeof(int));
	set_c = calloc(array_size, sizeof(int));
    set_r = calloc(array_size, sizeof(int));
    set_u = calloc(array_size, sizeof(int));
    set_m = calloc(array_size, sizeof(int));
    

	for (int i = 0; i < array_size; ++i)
	{
        set_i[i] = i+1;
		set_c[i] = i+1;
        set_u[i] = -1;
        set_m[i] = -1;//array_size-i;
        set_r[i] = -1;
	}

	//initialize initial variables
	p = key_1 * key_2;
	y = 1;
	z = 1;

	for (int i = 0; i < array_size; ++i)
	{
		w = y + (key_2 + i);
		x = z + (key_1 + i);
		y = y + w;
		z = x + y + z;
		p = (p + y + z) % array_size;
		//set_c[i] = (i+1);
        //set_c[i] = abs(p);
        set_c[i] = p;
	}

	//find duplicate and flag it
    //in set c as -1
    int u_array_size = array_size;
    int u_counter = 0;
	for (int i = 0; i < array_size; ++i)
	{
		int c_count = 0;
		for (int j = 0; j < array_size; ++j)
		{
            //check if current number is duped
			if (set_c[i] == set_c[j] && set_c[j] != -1)
            {
				++c_count;
                if (c_count > 1)
                {
                    set_c[j] = -1;
                    --u_array_size;
                    //put index of j into u
                    //for later, when
                    //inserting missing nums
                    set_u[u_counter] = j;
                    ++u_counter;
                }
            }
		}
	}
    
    //build set u and set r from set c
    //if a value is 0, also flag it as missing / -1
    for (int i = 0; i < array_size; ++i)
    {
        set_u[i]=set_c[i];
        
        if (set_c[i]==0)
            set_u[i]=-1;
        set_r[i] = set_u[i];
    }
    
    
    
    //count missing values
    //and add indexes of missing value to m
    int missing_nums=0;
    for (int i = 0; i < array_size; ++i)
    {
        if (set_u[i]==-1)
        {
            set_m[missing_nums]=i;
            ++missing_nums;
        }
    }
    
    //sort u into r temporarily
    qsort(set_r, array_size, sizeof(int), compare);
    
    //sort flagged indices
    //qsort(set_u, u_counter, sizeof(int), compare);
    
    //compare between i + missingnum and array size for
    //set r and set i
    int m_offset=missing_nums-1;
    for (int i=0; i < array_size; ++i)
    {
        int found=0;
        for (int j=missing_nums; j < array_size; ++j)
        {
            if (set_r[j]==set_i[i])
            {
                found++;
            }
        }
        
        if (found==0)
        {
            set_c[set_m[m_offset]]=set_i[i];
            --m_offset;
        }
    }
    
    //populate missing element
    //from set u using count of set m
    //stored in set r
    
    
    
    //populate missing element
    //from set u
    /*int m_counter = 0;
    int x_counter = 1;
    for (int i = 0; i < array_size; ++i)
    {
        int found = 0;
        int sum_method_count = 0;
        for (int j = 0; j < array_size ; ++j)
        {
            if (set_c[j] == set_m[i] && set_c[j] != -1)
            {
                ++found;
                break;
            }
            else
            {
                ++sum_method_count;
            }
        }
        
        //complete number would yield array_size - 1 result
        if (found == 0 && m_counter < u_counter)
        {
            set_c[set_u[m_counter]] = set_m[i];
            ++m_counter;
        }
        else if (found == 0 && set_c[i]==0)
        {
            //append
            //set_c[i] = set_m[i];
        }
    }*/
    
    
    //init array if not yet initialized
    if (output_array == NULL)
    {
        output_array = calloc(array_size, sizeof(int));
    }
    
    //copy to output
    //and reduce by 1 for 0-based indexing
    for (int i = 0; i < array_size; ++i)
    {
        output_array[i] = set_c[i]-1;
    }
    
    free(set_c);
    free(set_u);
    free(set_m);
    free(set_r);
    free(set_i);
}

int is_table_unique(int* output_array, int count)
{
    int* check_list = NULL;
    check_list = calloc(count, sizeof(int));
    for (int i=0; i < count; ++i)
    {
        check_list[i]=i;
    }
    
    //verify uniqueness
    int dup_detected = 0;
    
    for (int i=0; i < count; ++i)
    {
        int num = 0;
        int dup_count = 0;
        if (dup_detected == 0)
        {
            for (int j = 0; j < count ; ++j)
            {
                if (output_array[i] == check_list[j])
                {
                    ++dup_count;
                    num = output_array[i];
                }
            }
        }
        
        if (dup_count > 1)
        {
            dup_detected = 1;
            dup_count = 0;
        }
        //printf("%d ",output_array[i]);
    }
    
    free(check_list);
    return dup_detected;
}


//128-bit Fibonacci line-feedback shift register keystream generator
void generate_128bit_fibonacci_keystream(const char* input_seed, unsigned char* output_keystream)
{
	//https://stackoverflow.com/questions/7830379/how-to-implement-128-bit-linear-feedback-shift-register-with-byte-element-array
	//how to shift 16 chars in a sequence
	//128 - 128 =   0   => byte  0 bit 0
	//128 -  29 =  99   => byte 12 bit 3
	//128 -  27 = 101   => byte 12 bit 5
	//128 -   2 = 126   => byte 15 bit 6
	//128 -   1 = 127   => byte 15 bit 7

	unsigned char bytes[16];
	for (int i = 0; i < 16; ++i)
	{
		bytes[i] = input_seed[i];
	}

	//max period for 128-bit LSFR
	double max_period = pow(2, 128) - 1;
	unsigned short lfsr = bytes[0];
	unsigned int bit;
	unsigned int period = 0;
	int bitcount = 0;

	do
	{
		bit = ((bytes[0] >> 0) ^ (bytes[12] >> 3) ^ (bytes[12] >> 5) ^ (bytes[15] >> 6) ^ (bytes[15] >> 7)) & 1;
		for (int i = 0; i < 16; ++i)
		{
			if (i<15)
				bytes[i] = (bytes[i] >> 1) | (bytes[i + 1] << 7);
			else
				bytes[i] = (bytes[i] >> 1) | (bit << 7);

		}
		++period;
	} while (period < 128);

	//copy to output
	for (int i = 0; i < 16; ++i)
	{
		output_keystream[i] = bytes[i];
	}

	//
}

void generate_1024bit_fibonacci_keystream(const char* input_seed, int input_length, int period_limit, unsigned char* output_keystream)
{
    //taps for 1024 bit LFSR ; 1024, 1015, 1002, 1001
    //how to shift 128 bytes in sequence
    //1024 - 1024 = 0; byte 0 bit 0
    //1024 - 1015 = 9; byte 1 bit 1
    //1024 - 1002 = 22; byte 2 bit 6
    //1024 - 1001 = 23; byte 2 bit 7
    
    unsigned char bytes[128];
    for (int i = 0; i < input_length; ++i)
    {
        bytes[i] = input_seed[i];
    }
    
    //max period for 1024-bit LSFR
    long long max_period = pow(2, 1024) - 1;
    unsigned int bit =0;
    unsigned int period = 0;
    int period_cap = 0;
    if (period_limit > max_period)
        period_cap = max_period;
    else
        period_cap = period_limit;
    
    
    do
    {
        bit = ((bytes[0] >> 0) ^ (bytes[1] >> 1) ^ (bytes[2] >> 6) ^ (bytes[2] >> 7)) & 1;
        for (int i = 0; i < input_length; ++i)
        {
            if (i<input_length)
                bytes[i] = (bytes[i] >> 1) | (bytes[i + 1] << 7);
            else
                bytes[i] = (bytes[i] >> 1) | (bit << 7);
            
        }
        ++period;
    } while (period < period_cap);
    
    //copy to output
    for (int i = 0; i < 128; ++i)
    {
        output_keystream[i] = bytes[i];
    }
    
}

//generate a P-box containing indexes of permuted original index, using four numbers as keys
//note: this algo has errors because of a prime miscalculation
void generate_permutation_box(unsigned int input_a, unsigned int input_b, unsigned int input_c, unsigned int input_d, unsigned int* output_array, unsigned int output_array_size)
{
    //Zobeiri, M., & Maybodi, B. M. N. (2006). INTRODUCING DYNAMIC P-BOX AND S-BOX BASED ON MODULAR CALCULATION AND KEY ENCRYPTION FOR ADDING TO CURRENT CRYPTOGRAPHIC SYSTEMS AGAINST THE LINEAR AND DIFFERENTIAL CRYPTANALYSIS.
    unsigned long a_prime = input_a + 1000;
    unsigned long b_prime = input_b + 1000;
    unsigned long c_prime = input_c + 100;
    unsigned long d_prime = input_d + 10;
    
    //get largest prime that is less than c
    unsigned long p_c = 0;
    unsigned int lowest_prime_index = 0;
    for (int i=primes_size; i > 0; --i)
    {
        if (primes[i] < c_prime)
        {
            p_c = primes[i];
            lowest_prime_index = i;
            break;
        }
    }
    
    //get smallest prime that is <= output array size
    unsigned long n_prime = 0;
    int minimum_prime_index = 0;
    int size_between_n_and_0 = 0;
    for (int i=0; i < primes_size; ++i)
    {
        if (output_array_size <= primes[i])
        {
            n_prime = primes[i];
            minimum_prime_index = i;
            size_between_n_and_0 = primes_size - i;
            break;
        }
    }
    
    //get list of primes smaller than output array size
    
    unsigned long* p_primes = calloc(lowest_prime_index, sizeof(unsigned long));
    for (int i=0; i < lowest_prime_index; ++i)
    {
        p_primes[i]=primes[lowest_prime_index - i];
    }
    
    
    //initialize counter m
    unsigned long m = output_array_size;
    
    //initialize temporary output
    unsigned int* output_block = calloc(output_array_size, sizeof(unsigned int));
    
    //start calculating
    for (int i=0; i < output_array_size; ++i)
    {
        unsigned long A = a_prime;
        int k_mod_D = i % d_prime;
        unsigned long P_k_mod_D = p_primes[k_mod_D];
        unsigned long B = b_prime;
        unsigned long N = primes[minimum_prime_index];
        unsigned long A_minus_P_k_mod_D = (A - (P_k_mod_D));
        unsigned long B_plus_P_k_mod_D = B + P_k_mod_D;
        unsigned long long powered = powmodp(A_minus_P_k_mod_D, B_plus_P_k_mod_D, N);
        unsigned int result = ((powered % output_array_size)%N)%m;
        output_block[i]=result;
        --m;
    }
    
    //copy to output
    for (int i=0; i < output_array_size; ++i)
    {
        output_array[i]=output_block[i];
    }
    
    //clear allocations
    free(output_block);
    free(p_primes);
}

void generate_substitution_box(const unsigned char* key, int key_length, unsigned char* output_box, unsigned int output_array_size)
{
	//Kazlauskas, K., Vaicekauskas, G., & Smaliukas, R. (2015). An algorithm for key-dependent S-box generation in block cipher system. Informatica, 26(1), 51-65.
	//unsigned char output_box[256] = { 0 };

	//compute j
	unsigned long long j = 0;
    for (int i = 0; i < key_length; ++i)
    {
        j += key[i] % 256;
    }
    
    //compute next j
	for (int i = 0; i < output_array_size; ++i)
	{
        //compute k
        unsigned long long k = 0;
        k = (output_box[i] +output_box[j])%key_length;
        j = (j + key[k])%256;
        //swap i with j
        unsigned long long p = 0;
        p = output_box[i];
        output_box[i]=output_box[j];
        output_box[j]=p;
	}
    
    



        
}

void initialize_aes_sbox(uint8_t sbox[256]) {
    uint8_t p = 1, q = 1;
    
    /* loop invariant: p * q == 1 in the Galois field */
    do {
        /* multiply p by 3 */
        p = p ^ (p << 1) ^ (p & 0x80 ? 0x1B : 0);
        
        /* divide q by 3 (equals multiplication by 0xf6) */
        q ^= q << 1;
        q ^= q << 2;
        q ^= q << 4;
        q ^= q & 0x80 ? 0x09 : 0;
        
        /* compute the affine transformation */
        //from formula : b XOR (b ROL 1) XOR (b ROL 2) XOR (b ROL 3) XOR (b ROL 4)
        uint8_t xformed = q ^ ROTL8(q, 1) ^ ROTL8(q, 2) ^ ROTL8(q, 3) ^ ROTL8(q, 4);
        
        sbox[p] = xformed ^ 0x63;
    } while (p != 1);
    
    /* 0 is a special case since it has no inverse */
    sbox[0] = 0x63;
}

void xor_transform(const unsigned char* input_data, unsigned int input_length, unsigned char* output_data, unsigned int output_length)
{
    for (int i=0; i < input_length; ++i)
    {
        output_data[i] = output_data[i] ^ input_data[i];
    }
}

unsigned char get_rijndael_forward_box_value(unsigned char input)
{
    //get nibbles from input
    unsigned char high_nibble = ((input) >> 4) & 0x0F;
    unsigned char low_nibble = input & 0x0F;
    //create the matrix
    unsigned char aes_matrix_forward[16][16]={
        {0x63 ,0x7c ,0x77 ,0x7b ,0xf2 ,0x6b ,0x6f ,0xc5 ,0x30 ,0x01 ,0x67 ,0x2b ,0xfe ,0xd7 ,0xab ,0x76}
        ,{0xca ,0x82 ,0xc9 ,0x7d ,0xfa ,0x59 ,0x47 ,0xf0 ,0xad ,0xd4 ,0xa2 ,0xaf ,0x9c ,0xa4 ,0x72 ,0xc0}
        ,{0xb7 ,0xfd ,0x93 ,0x26 ,0x36 ,0x3f ,0xf7 ,0xcc ,0x34 ,0xa5 ,0xe5 ,0xf1 ,0x71 ,0xd8 ,0x31 ,0x15}
        ,{0x04 ,0xc7 ,0x23 ,0xc3 ,0x18 ,0x96 ,0x05 ,0x9a ,0x07 ,0x12 ,0x80 ,0xe2 ,0xeb ,0x27 ,0xb2 ,0x75}
        ,{0x09 ,0x83 ,0x2c ,0x1a ,0x1b ,0x6e ,0x5a ,0xa0 ,0x52 ,0x3b ,0xd6 ,0xb3 ,0x29 ,0xe3 ,0x2f ,0x84}
        ,{0x53 ,0xd1 ,0x00 ,0xed ,0x20 ,0xfc ,0xb1 ,0x5b ,0x6a ,0xcb ,0xbe ,0x39 ,0x4a ,0x4c ,0x58 ,0xcf}
        ,{0xd0 ,0xef ,0xaa ,0xfb ,0x43 ,0x4d ,0x33 ,0x85 ,0x45 ,0xf9 ,0x02 ,0x7f ,0x50 ,0x3c ,0x9f ,0xa8}
        ,{0x51 ,0xa3 ,0x40 ,0x8f ,0x92 ,0x9d ,0x38 ,0xf5 ,0xbc ,0xb6 ,0xda ,0x21 ,0x10 ,0xff ,0xf3 ,0xd2}
        ,{0xcd ,0x0c ,0x13 ,0xec ,0x5f ,0x97 ,0x44 ,0x17 ,0xc4 ,0xa7 ,0x7e ,0x3d ,0x64 ,0x5d ,0x19 ,0x73}
        ,{0x60 ,0x81 ,0x4f ,0xdc ,0x22 ,0x2a ,0x90 ,0x88 ,0x46 ,0xee ,0xb8 ,0x14 ,0xde ,0x5e ,0x0b ,0xdb}
        ,{0xe0 ,0x32 ,0x3a ,0x0a ,0x49 ,0x06 ,0x24 ,0x5c ,0xc2 ,0xd3 ,0xac ,0x62 ,0x91 ,0x95 ,0xe4 ,0x79}
        ,{0xe7 ,0xc8 ,0x37 ,0x6d ,0x8d ,0xd5 ,0x4e ,0xa9 ,0x6c ,0x56 ,0xf4 ,0xea ,0x65 ,0x7a ,0xae ,0x08}
        ,{0xba ,0x78 ,0x25 ,0x2e ,0x1c ,0xa6 ,0xb4 ,0xc6 ,0xe8 ,0xdd ,0x74 ,0x1f ,0x4b ,0xbd ,0x8b ,0x8a}
        ,{0x70 ,0x3e ,0xb5 ,0x66 ,0x48 ,0x03 ,0xf6 ,0x0e ,0x61 ,0x35 ,0x57 ,0xb9 ,0x86 ,0xc1 ,0x1d ,0x9e}
        ,{0xe1 ,0xf8 ,0x98 ,0x11 ,0x69 ,0xd9 ,0x8e ,0x94 ,0x9b ,0x1e ,0x87 ,0xe9 ,0xce ,0x55 ,0x28 ,0xdf}
        ,{0x8c ,0xa1 ,0x89 ,0x0d ,0xbf ,0xe6 ,0x42 ,0x68 ,0x41 ,0x99 ,0x2d ,0x0f ,0xb0 ,0x54 ,0xbb ,0x16}};
    return aes_matrix_forward[high_nibble][low_nibble];
}

unsigned char get_rijndael_inverse_box_value(unsigned char input)
{
    //get nibbles from input
    unsigned char high_nibble = ((input) >> 4) & 0x0F;
    unsigned char low_nibble = input & 0x0F;
    //create the matrix
    unsigned char aes_matrix_forward[16][16]={
        {0x52 ,0x09 ,0x6a ,0xd5 ,0x30 ,0x36 ,0xa5 ,0x38 ,0xbf ,0x40 ,0xa3 ,0x9e ,0x81 ,0xf3 ,0xd7 ,0xfb}
        ,{0x7c ,0xe3 ,0x39 ,0x82 ,0x9b ,0x2f ,0xff ,0x87 ,0x34 ,0x8e ,0x43 ,0x44 ,0xc4 ,0xde ,0xe9 ,0xcb}
        ,{0x54 ,0x7b ,0x94 ,0x32 ,0xa6 ,0xc2 ,0x23 ,0x3d ,0xee ,0x4c ,0x95 ,0x0b ,0x42 ,0xfa ,0xc3 ,0x4e}
        ,{0x08 ,0x2e ,0xa1 ,0x66 ,0x28 ,0xd9 ,0x24 ,0xb2 ,0x76 ,0x5b ,0xa2 ,0x49 ,0x6d ,0x8b ,0xd1 ,0x25}
        ,{0x72 ,0xf8 ,0xf6 ,0x64 ,0x86 ,0x68 ,0x98 ,0x16 ,0xd4 ,0xa4 ,0x5c ,0xcc ,0x5d ,0x65 ,0xb6 ,0x92}
        ,{0x6c ,0x70 ,0x48 ,0x50 ,0xfd ,0xed ,0xb9 ,0xda ,0x5e ,0x15 ,0x46 ,0x57 ,0xa7 ,0x8d ,0x9d ,0x84}
        ,{0x90 ,0xd8 ,0xab ,0x00 ,0x8c ,0xbc ,0xd3 ,0x0a ,0xf7 ,0xe4 ,0x58 ,0x05 ,0xb8 ,0xb3 ,0x45 ,0x06}
        ,{0xd0 ,0x2c ,0x1e ,0x8f ,0xca ,0x3f ,0x0f ,0x02 ,0xc1 ,0xaf ,0xbd ,0x03 ,0x01 ,0x13 ,0x8a ,0x6b}
        ,{0x3a ,0x91 ,0x11 ,0x41 ,0x4f ,0x67 ,0xdc ,0xea ,0x97 ,0xf2 ,0xcf ,0xce ,0xf0 ,0xb4 ,0xe6 ,0x73}
        ,{0x96 ,0xac ,0x74 ,0x22 ,0xe7 ,0xad ,0x35 ,0x85 ,0xe2 ,0xf9 ,0x37 ,0xe8 ,0x1c ,0x75 ,0xdf ,0x6e}
        ,{0x47 ,0xf1 ,0x1a ,0x71 ,0x1d ,0x29 ,0xc5 ,0x89 ,0x6f ,0xb7 ,0x62 ,0x0e ,0xaa ,0x18 ,0xbe ,0x1b}
        ,{0xfc ,0x56 ,0x3e ,0x4b ,0xc6 ,0xd2 ,0x79 ,0x20 ,0x9a ,0xdb ,0xc0 ,0xfe ,0x78 ,0xcd ,0x5a ,0xf4}
        ,{0x1f ,0xdd ,0xa8 ,0x33 ,0x88 ,0x07 ,0xc7 ,0x31 ,0xb1 ,0x12 ,0x10 ,0x59 ,0x27 ,0x80 ,0xec ,0x5f}
        ,{0x60 ,0x51 ,0x7f ,0xa9 ,0x19 ,0xb5 ,0x4a ,0x0d ,0x2d ,0xe5 ,0x7a ,0x9f ,0x93 ,0xc9 ,0x9c ,0xef}
        ,{0xa0 ,0xe0 ,0x3b ,0x4d ,0xae ,0x2a ,0xf5 ,0xb0 ,0xc8 ,0xeb ,0xbb ,0x3c ,0x83 ,0x53 ,0x99 ,0x61}
        ,{0x17 ,0x2b ,0x04 ,0x7e ,0xba ,0x77 ,0xd6 ,0x26 ,0xe1 ,0x69 ,0x14 ,0x63 ,0x55 ,0x21 ,0x0c ,0x7d}};
    return aes_matrix_forward[high_nibble][low_nibble];
}

void permute_forward_16bits(unsigned char* inout_data, unsigned long data_size, int perm_boxes[16], int box_key_count)
{
    int key_counter = 0;
    key_counter = box_key_count;
    for (int i=0; i < data_size; i=i+2)
    {
        unsigned char p1 = inout_data[i];
        unsigned char p2 = inout_data[i+1];
        //[0][1][2][3][4][5][6][7][0][1][2][3][4][5][6][7]
        //[0][1][2][3][4][5][6][7][8][9][10][11][12][13][14][15]
        for (int j=0; j < 16; ++j)
        {
            int perm_bit_index = perm_boxes[j];
            
            if (j < 8)
            {
                
                int bitval = 0;
                bitval = get_char_bit_value(j, p1);
                printf("%d",bitval);
                //move bit in position j to bit in position perm_bit_index
                if (bitval==1)
                {
                    if (perm_bit_index < 8)
                    {
                        //printf("A%d -> A%d [%d]\n", j,perm_bit_index,bitval);
                        set_char_bit(perm_bit_index, &inout_data[i]);
                    }
                    else
                    {
                        //printf("A%d -> B%d [%d]\n", j,perm_bit_index-8,bitval);
                        set_char_bit(perm_bit_index-8, &inout_data[i+1]);
                    }
                    
                }
                else
                {
                    if (perm_bit_index < 8)
                    {
                        //printf("A%d -> A%d [%d]\n", j,perm_bit_index,bitval);
                        clear_char_bit(perm_bit_index, &inout_data[i]);
                    }
                    else
                    {
                        //printf("A%d -> B%d [%d]\n", j,perm_bit_index-8,bitval);
                        clear_char_bit(perm_bit_index-8, &inout_data[i+1]);
                    }
                }
                
            }
            else
            {
                int bitval = 0;
                bitval = get_char_bit_value(j-8, p2);
                printf("%d",bitval);
                //move bit in position j to bit in position perm_bit_index
                if (bitval==1)
                {
                    if (perm_bit_index < 8)
                    {
                        //printf("B%d -> A%d [%d]\n", j-8,perm_bit_index,bitval);
                        set_char_bit(perm_bit_index, &inout_data[i]);
                    }
                    else
                    {
                        //printf("B%d -> B%d [%d]\n", j-8,perm_bit_index-8,bitval);
                        set_char_bit(perm_bit_index-8, &inout_data[i+1]);
                    }
                    
                }
                else
                {
                    if (perm_bit_index < 8)
                    {
                        //printf("B%d -> A%d [%d]\n", j-8,perm_bit_index,bitval);
                        clear_char_bit(perm_bit_index, &inout_data[i]);
                    }
                    else
                    {
                        //printf("B%d -> B%d [%d]\n", j-8,perm_bit_index-8,bitval);
                        clear_char_bit(perm_bit_index-8, &inout_data[i+1]);
                    }
                }
            }            
        }
        printf("\n");
    }

}
