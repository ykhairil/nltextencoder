//
//  keygen.h
//  NLTextEncoder
//
//  Created by yussairi on 01/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef keygen_h
#define keygen_h

#include <stdio.h>
#include <stdint.h>
unsigned long long get_biggest_prime_base();
unsigned long long get_prime_base(int index);
void get_diffie_hellman_64bit_public_key(unsigned long long prime_base, unsigned long long prime_modulus, unsigned long long secret_a, unsigned long long secret_b, unsigned long long* public_key_A, unsigned long long* public_key_B);
unsigned long long get_diffie_hellman_64bit_private_key(unsigned long long public_key, unsigned long long secret_key, unsigned long long prime_base);
void generate_2kdp_permutation(int array_size, int* output_array, unsigned long long key_1, unsigned long long key_2);
int is_table_unique(int* output_array, int count);
void generate_128bit_fibonacci_keystream(const char* input_seed, unsigned char* output_keystream);
void generate_1024bit_fibonacci_keystream(const char* input_seed, int input_length, int period_limit, unsigned char* output_keystream);
void generate_permutation_box(unsigned int input_a, unsigned int input_b, unsigned int input_c, unsigned int input_d, unsigned int* output_array, unsigned int output_array_size);
int compare (const void * a, const void * b);
void initialize_aes_sbox(uint8_t sbox[256]);
void generate_substitution_box(const unsigned char* key, int key_length, unsigned char* output_box, unsigned int output_array_size);
unsigned char get_rijndael_forward_box_value(unsigned char input);
unsigned char get_rijndael_inverse_box_value(unsigned char input);
void permute_forward_16bits(unsigned char* inout_data, unsigned long data_size, int perm_boxes[16], int box_key_count);
#endif /* keygen_h */
