//
//  main.c
//  NLTextEncoder
//
//  Created by yussairi on 12/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mynlcoder.h"
#include "mynltests.h"
#include "keygentests.h"
#include "mynllexertests.h"
#include "myalgotests.h"
#include "mycodectests.h"

void run_test()
{
	run_all_mycodec_tests();
    run_all_lexer_tests();
    run_all_mynl_test();
    run_all_keygen_test();
    run_all_algo_test();
}

void show_status(unsigned long error)
{
    if (error==error_input_read)
    {
        printf("Unable to properly read the source file.\n");
    }
    if (error==error_input_open)
    {
        printf("Unable to open the source file for reading.\n");
    }
    if (error==error_empty_input)
    {
        printf("Source file is empty.\n");
    }
    if (error==error_output_open)
    {
        printf("Unable to open the destination file for writing.\n");
    }
    if (error==error_output_write)
    {
        printf("Error writing to destination file.\n");
    }
    if (error==error_empty_output)
    {
        printf("Decompression failed.\n");
    }
}

void parse_commandline_options(int argc, const char * argv[])
{
    //format should be [-s] [string|path] [list of options]
    //empty arg would return help printout
    if (argc == 1)
    {
        printf("Usage: nltextencoder [source] [destination] [-d] [-c] [-r] [-v] \n\n");
        printf("-d  Specify a decoding operation on the source file; when omitted, operation is considered an encoding \n");
        printf("-c  Use capitalization ignoring mode during encode/decode operation \n");
        printf("-r  Use code-width reduction mode during encode/decode operation \n");
        printf("-v  Enable verbose information during operation \n");
    }
    else
    {
        int do_decode = 0;
        int do_use_lossy_caps_ignore_mode = 0;
        int do_use_lossless_code_reduction_mode = 0;
        int do_verbose_operation = 0;
        
        //get the optional argument first
        if (argc > 3)
        {
            
            for (int i=3; i < argc; ++i)
            {
                if (strcmp(argv[i],"-d") == 0)
                {
                    do_decode=1;
                }
                else if (strcmp(argv[i],"-c") == 0)
                {
                    do_use_lossy_caps_ignore_mode=1;
                }
                else if (strcmp(argv[i],"-r") == 0)
                {
                    do_use_lossless_code_reduction_mode=1;
                }
                else if (strcmp(argv[i],"-v") == 0)
                {
                    do_verbose_operation=1;
                }

            }
        }
        
        //get the src and dest files
        if (argc > 2)
        {
            if (do_use_lossy_caps_ignore_mode == 1)
                toggle_lossy_ignore_capitalization();
            if (do_use_lossless_code_reduction_mode == 1)
                toggle_lossy_reduce_symbol_width();
            unsigned long status = 0;
            clock_t t;
            t = clock();
            if (do_decode==0)
            {
                status = compress_mynl_file(argv[1], argv[2]);
            }
            else
            {
                status = decompress_mynl_file(argv[1], argv[2]);
            }
            t = clock() - t;
            double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
            show_status(status);
            printf("Duration : %f seconds \n", time_taken);
        }
        else
        {
            //purposely unspecified option for running tests of a build
            if (strcmp(argv[1], "-t") == 0)
            {
                //run tests
                run_test();
            }
            else
            {
                //not enough arguments
                printf("Not enough parameters. At least both source file path and destination file path must be provided. \n");
            }
            
        }
        
    }
}


int main(int argc, const char * argv[]) {
    // insert code here...
    printf("NLTextEncoder v 1.0 by Yussairi Khairil\n");
    //test();
    //run_test();
    parse_commandline_options(argc, argv);
    return 0;
}
