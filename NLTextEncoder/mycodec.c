//
//  mycodec.c
//  NLTextEncoder
//
//  Created by yussairi on 22/01/2020.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mycodec.h"
//#include "mynlsymbols.h"
//#include "bitset.h"
//#include "keygen.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


//3.3.1
int encode(const char* i_secret_message, int i_input_size, unsigned char* o_output_bits, int* o_output_size)
{
	int start_of_substring = 0;
	int end_of_substring = 0;
	int search_position = 0;

	char substring[64] = {0};

	int caps_code_output[64] = { 0 };
	int identifier_code_output[64] = { 0 };
	int symbol_code_output[64] = { 0 };
	int output_size = 0;


	//BEGIN
	end_of_substring = i_input_size;
	search_position = start_of_substring;

	while (start_of_substring != end_of_substring)
	{
		char terminus_candidate_1c = i_secret_message[search_position];
		for (int i = 0; i < mycodec_1grams_terminals_size; ++i)
		{
			if (terminus_candidate_1c == mycodec_1grams_terminals[i][0])
			{
				//prepare temporary cache to reverse later
				int caps_code_output_to_reverse[64] = { 0 };
				int identifier_code_output_to_reverse[64] = { 0 };
				int symbol_code_output_to_reverse[64] = { 0 };
				int output_size_to_reverse = 0;

				int substring_len = search_position - start_of_substring;
				memcpy(substring, i_secret_message + start_of_substring, substring_len);
				encode_word(substring, substring_len, &caps_code_output_to_reverse, &identifier_code_output_to_reverse, &symbol_code_output_to_reverse, &output_size_to_reverse);
				
				//reverse extracted code and copy to output
				for (int i = 0; i < output_size_to_reverse; ++i)
				{
					caps_code_output[output_size + i] = caps_code_output_to_reverse[output_size_to_reverse - 1 - i];
					identifier_code_output[output_size + i] = identifier_code_output_to_reverse[output_size_to_reverse - 1 - i];
					symbol_code_output[output_size + i] = symbol_code_output_to_reverse[output_size_to_reverse - 1 - i];
				}
				output_size += output_size_to_reverse;

				//add terminus code to output
				caps_code_output[output_size] = 0;
				identifier_code_output[output_size] = terminal1;
				symbol_code_output[output_size] = i;
				++output_size;

				start_of_substring = search_position + 1;
				memset(substring, 0, 64);
				break;
			}
			else
			{
				
			}
		}
		//++start_of_substring;
		++search_position;
	};
	return 0;
}

int encode_word(const char* i_word, int i_input_size, int* o_capitalization_codes, int* o_identifier_codes, int* o_symbol_codes, int* io_output_size)
{
	int start_of_substring = 0;
	int end_of_substring = 0;

	int caps_code = 0;
	
	int is_word_found = 0;

	for (int i = 0; i < mycodec_commons_size; ++i)
	{
		if (strcmp(i_word, mycodec_commons[i]) == 0)
		{
			//get casing style
			encode_capitalization(i_word, i_input_size, &caps_code);
			//add to entry of each data at index io_output_size
			o_identifier_codes[*io_output_size] = commonwords;
			o_capitalization_codes[*io_output_size] = caps_code;
			o_symbol_codes[*io_output_size] = i;
			//increment io_output_size
			++(*io_output_size);
			is_word_found = 1;
			break;
		}
		else
		{
			
		}
	}

	//stem word further
	if (!is_word_found)
		encode_stem(i_word, i_input_size, 0, i_input_size, o_capitalization_codes, o_identifier_codes, o_symbol_codes, io_output_size);

	return 0;
}

int encode_stem(const char* i_word, int i_input_size, int i_substring_start, int i_substring_end, int* o_capitalization_codes, int* o_identifier_codes, int* o_symbol_codes, int* io_output_size)
{
	int syllable_length = 5;
	int search_position = i_substring_start;
	int subsubstring_start = i_substring_start;
	int subsubstring_end = i_input_size;
	char subsubstring[6] = { 0 };
	int is_stem_found = 0;
	int caps_code = 0;

	if (subsubstring_end - subsubstring_start < syllable_length)
		subsubstring_start = 0;
	else
	{
		subsubstring_start = subsubstring_end - syllable_length;
	}
		
	while (subsubstring_end > search_position)
	{
		int subsubstring_len =  (subsubstring_end) - subsubstring_start;
		memcpy(subsubstring, i_word + subsubstring_start, subsubstring_len);

		if (syllable_length == 5)
		{
			for (int i = 0; i < mycodec_5grams_syllable_size; ++i)
			{
				if (strcmp(subsubstring, mycodec_5grams_syllable[i]) == 0)
				{
					//get casing style
					encode_capitalization(subsubstring, subsubstring_len, &caps_code);
					//add to entry of each data at index io_output_size
					o_identifier_codes[*io_output_size] = gram5;
					o_capitalization_codes[*io_output_size] = caps_code;
					o_symbol_codes[*io_output_size] = i;
					//increment io_output_size
					++(*io_output_size);
					is_stem_found = 1;
					syllable_length=5;
					subsubstring_end = subsubstring_start;
					subsubstring_start = subsubstring_end - syllable_length;
					if (subsubstring_start < 0)
						subsubstring_start = 0;
					break;
				}
			}

		}
		else if (syllable_length == 4)
		{
			for (int i = 0; i < mycodec_4grams_syllable_size; ++i)
			{
				if (strcmp(subsubstring, mycodec_4grams_syllable[i]) == 0)
				{
					//get casing style
					encode_capitalization(subsubstring, subsubstring_len, &caps_code);
					//add to entry of each data at index io_output_size
					o_identifier_codes[*io_output_size] = gram4;
					o_capitalization_codes[*io_output_size] = caps_code;
					o_symbol_codes[*io_output_size] = i;
					//increment io_output_size
					++(*io_output_size);
					is_stem_found = 1;
					syllable_length = 5;
					subsubstring_end = subsubstring_start;
					subsubstring_start = subsubstring_end - syllable_length;
					if (subsubstring_start < 0)
						subsubstring_start = 0;
					break;
				}
			}
		}
		else if (syllable_length == 3)
		{
			for (int i = 0; i < mycodec_3grams_syllable_size; ++i)
			{
				if (strcmp(subsubstring, mycodec_3grams_syllable[i]) == 0)
				{
					//get casing style
					encode_capitalization(subsubstring, subsubstring_len, &caps_code);
					//add to entry of each data at index io_output_size
					o_identifier_codes[*io_output_size] = gram3;
					o_capitalization_codes[*io_output_size] = caps_code;
					o_symbol_codes[*io_output_size] = i;
					//increment io_output_size
					++(*io_output_size);
					is_stem_found = 1;
					syllable_length = 5;
					subsubstring_end = subsubstring_start;
					subsubstring_start = subsubstring_end - syllable_length;
					if (subsubstring_start < 0)
						subsubstring_start = 0;
					break;
				}
			}
		}
		else if (syllable_length == 2)
		{
			for (int i = 0; i < mycodec_2grams_syllable_size; ++i)
			{
				if (strcmp(subsubstring, mycodec_2grams_syllable[i]) == 0)
				{
					//get casing style
					encode_capitalization(subsubstring, subsubstring_len, &caps_code);
					//add to entry of each data at index io_output_size
					o_identifier_codes[*io_output_size] = gram2;
					o_capitalization_codes[*io_output_size] = caps_code;
					o_symbol_codes[*io_output_size] = i;
					//increment io_output_size
					++(*io_output_size);
					is_stem_found = 1;
					syllable_length = 5;
					subsubstring_end = subsubstring_start;
					subsubstring_start = subsubstring_end - syllable_length;
					if (subsubstring_start < 0)
						subsubstring_start = 0;
					break;
				}
			}
		}

		if (!is_stem_found)
		{
			--syllable_length;
			subsubstring_start = subsubstring_end - syllable_length;
		}
		else
		{
			is_stem_found = 0;			
		}


		memset(subsubstring, 0, 6);
	}

	return 0;
}

int encode_capitalization(const char* i_word, int i_input_size, int* o_capitalization)
{
	int result = 0;
	int uppercase_count = 0;
	int lowercase_count = 0;
	for (unsigned long i = 0; i < i_input_size; ++i)
	{
		if (isupper(i_word[i]))
			++uppercase_count;
		else
			++lowercase_count;
	}

	if (uppercase_count == i_input_size)
	{
		//AAA
		result = 0;
	}
	else if (isupper(i_word[0]) && uppercase_count == 1)
	{
		//Aaa
		result = 1;
	}
	else if (isupper(i_word[i_input_size - 1]) && uppercase_count == 1)
	{
		//aaA
		result = 2;
	}
	else if (lowercase_count == i_input_size)
	{
		//aaa
		result = 3;
	}
	*o_capitalization = result;
	return result;
}

int generate_binary(int* i_capitalizations, int* i_identifiers, int* i_codes, int i_input_size, unsigned char* o_output_bits, int* o_output_size)
{

	return 0;
}