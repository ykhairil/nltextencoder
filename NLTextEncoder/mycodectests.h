//
//  mynlcodectests.h
//  NLTextEncoder
//
//  Created by yussairi on 12/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef mycodectests_h
#define mycodectests_h

#include <stdio.h>

void run_all_mycodec_tests();

#endif /* mynlcodectests_h */
