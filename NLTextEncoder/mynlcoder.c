//
//  mynlcoder.c
//  NLTextEncoder
//
//  Created by yussairi on 12/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mynlcoder.h"
#include "mynlsymbols.h"
#include "bitset.h"
#include "keygen.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct mynl_code_table
{
    unsigned long original_index;
    unsigned long type;
    unsigned long code;
} nl_code_table;

int verbose_debug_mode=0;
int lossy_ignore_capitalization_mode=0;
int lossy_reduce_symbol_width_mode=0;
int keyed_symbols_mode=0;
nl_code_table* keyed_table = NULL;
unsigned int* keyed_commons_code_table=NULL;
unsigned int* keyed_5grams_code_table=NULL;
unsigned int* keyed_4grams_code_table=NULL;
unsigned int* keyed_3grams_code_table=NULL;
unsigned int* keyed_2grams_code_table=NULL;
unsigned int* keyed_2terminal_code_table=NULL;
unsigned int* keyed_1terminal_code_table=NULL;
unsigned int* keyed_uncoded_code_table=NULL;

#pragma mark private functions

//return the capitalization enum of string input_string
int get_capitalization_style(const char* input_string, unsigned long input_length)
{
    int result = 0;
    int uppercase_count = 0;
    int lowercase_count = 0;
    for (unsigned long i=0; i < input_length; ++i)
    {
        if (isupper(input_string[i]))
            ++uppercase_count;
        else
            ++lowercase_count;
    }
    
    if (uppercase_count == input_length)
    {
        //AAA
        result = 0;
    }
    else if (isupper(input_string[0]) && uppercase_count == 1)
    {
        //Aaa
        result = 1;
    }
    else if (isupper(input_string[input_length-1]) && uppercase_count == 1)
    {
        //aaA
        result = 2;
    }
    else if (lowercase_count == input_length)
    {
        //aaa
        result = 3;
    }
    
    return result;
}

//alter string output_string based on the specified capitalization style input_capstyle
//output - output_string
void set_capitalization_style(int input_capstyle, unsigned long output_length, char* output_string)
{
    if (input_capstyle==0)
    {
        for (unsigned long i=0; i < output_length; ++i)
        {
            output_string[i] = toupper(output_string[i]);
        }
    }
    else if (input_capstyle==1)
    {
		for (unsigned long i = 0; i < output_length; ++i)
        {
            if (i==0)
                output_string[i] = toupper(output_string[i]);
            else
                output_string[i] = tolower(output_string[i]);
        }
    }
    else if (input_capstyle==2)
    {
		for (unsigned long i = 0; i < output_length; ++i)
        {
            if (i==output_length-1)
                output_string[i] = toupper(output_string[i]);
            else
                output_string[i] = tolower(output_string[i]);
        }
    }
    else if (input_capstyle==3)
    {
		for (unsigned long i = 0; i < output_length; ++i)
        {
            output_string[i] = tolower(output_string[i]);
        }
    }
    else
    {
        
    }
}

int extract_common_words(const char* input_string, unsigned long original_substring_length, int* output_found_array, int* output_found_category, int* output_found_capstyle, unsigned long* output_find_count)
{
	//attempt to find common words
	int found_index = -1;

	//create array of string the length of input string and the size of input
	unsigned long input_size = original_substring_length+1;
	unsigned long next_substring_length = input_size;

	//sanity check
	if (input_size == 0)
		return -1;

	char* candidate = NULL;
	candidate = calloc(input_size + 1, sizeof(char));
	memcpy(candidate, input_string, input_size);
	candidate[input_size] = 0;

	//create a lowercased string for lookup against sylable list
	char* lowercased_candidate = NULL;
	lowercased_candidate = calloc(input_size + 1, sizeof(char));
	memcpy(lowercased_candidate, input_string, input_size);
	for (unsigned long i = 0; i < input_size; ++i)
	{
		lowercased_candidate[i] = tolower(lowercased_candidate[i]);
	}
	lowercased_candidate[input_size] = 0;

	unsigned long symbol_size = 0;
	symbol_size = mynl_commons_size;

	//use bitwidth reduction if it is set
	if (lossy_reduce_symbol_width_mode == 1)
		symbol_size = mynl_commons_reduced_size;

	for (int i = 0; i < symbol_size; ++i)
	{
		if (strcmp(lowercased_candidate, mynl_commons[i]) == 0)
		{
			found_index = i;			
			output_found_array[*output_find_count] = i;
			output_found_category[*output_find_count] = commonwords;
			output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
			(*output_find_count)++;			
			if (verbose_debug_mode == 1)
				printf(" > %s", candidate);
			break;
		}
	}

	free(candidate);
	free(lowercased_candidate);

	return found_index;
}

int extract_common_words_keyed(const char* input_string, unsigned long original_substring_length, int* output_found_array, int* output_found_category, int* output_found_capstyle, unsigned long* output_find_count)
{
    //attempt to find common words
    int found_index = -1;
    
    //create array of string the length of input string and the size of input
    unsigned long input_size = original_substring_length+1;
    unsigned long next_substring_length = input_size;
    
    //sanity check
    if (input_size == 0)
        return -1;
    
    char* candidate = NULL;
    candidate = calloc(input_size + 1, sizeof(char));
    memcpy(candidate, input_string, input_size);
    candidate[input_size] = 0;
    
    //create a lowercased string for lookup against sylable list
    char* lowercased_candidate = NULL;
    lowercased_candidate = calloc(input_size + 1, sizeof(char));
    memcpy(lowercased_candidate, input_string, input_size);
    for (unsigned long i = 0; i < input_size; ++i)
    {
        lowercased_candidate[i] = tolower(lowercased_candidate[i]);
    }
    lowercased_candidate[input_size] = 0;
    
    unsigned long symbol_size = 0;
    symbol_size = mynl_commons_size;
    
    //use bitwidth reduction if it is set
    if (lossy_reduce_symbol_width_mode == 1)
        symbol_size = mynl_commons_reduced_size;
    
    for (int i = 0; i < symbol_size; ++i)
    {
        if (strcmp(lowercased_candidate, mynl_commons[keyed_commons_code_table[i]]) == 0)
        {
            found_index = i;
            output_found_array[*output_find_count] = i;
            output_found_category[*output_find_count] = commonwords;
            output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
            (*output_find_count)++;
            if (verbose_debug_mode == 1)
                printf(" > %s", candidate);
            break;
        }
    }
    
    free(candidate);
    free(lowercased_candidate);
    
    return found_index;
}

//recursively stem out syllables from input_string in non-destructive way
//output - output_found_array, output_found_category, output_found_capstyle
//          , output_find_count, output_remaining_unprocessed_substring_length
//return - -1 when substring is processed completely
int stem_grams(const char* input_string, unsigned long original_substring_length, unsigned long input_search_start_position, unsigned long input_search_end_position, int* output_found_array, int* output_found_category, int* output_found_capstyle, unsigned long* output_find_count, unsigned long* output_remaining_unprocessed_substring_length)
{
	//gradually stem out ngrams in a word
	int found_index = -1;
	unsigned long found_ngram_length = 0;
    unsigned long next_input_search_start_position = input_search_start_position;
    unsigned long next_input_search_end_position = input_search_end_position;
    
	//create array of string the length of input string and the size of input
    unsigned long input_size = (input_search_end_position - input_search_start_position)+1;
    unsigned long next_substring_length = input_size;
    
	//sanity check
    if (input_size == 0)
        return -1;
    
	char* candidate = NULL;
	candidate = calloc(input_size+1, sizeof(char));
	memcpy(candidate, input_string + input_search_start_position, input_size+1);
	candidate[input_size] = 0;
    
    //create a lowercased string for lookup against sylable list
    char* lowercased_candidate = NULL;
    lowercased_candidate = calloc(input_size + 1, sizeof(char));
    memcpy(lowercased_candidate, input_string + input_search_start_position, input_size+1);
	for (unsigned long i = 0; i < input_size; ++i)
    {
        lowercased_candidate[i]=tolower(lowercased_candidate[i]);
    }
    lowercased_candidate[input_size] = 0;

	if (input_size == 5)
	{
        unsigned long symbol_size = 0;
        symbol_size = mynl_5grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_5grams_syllable_reduced_size;
        
		for (int i = 0; i < symbol_size; ++i)
		{
			if (strcmp(lowercased_candidate, mynl_5grams_syllable[i]) == 0)
			{
				found_index = i;
                found_ngram_length=5;
				output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram5;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
				(*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
				break;
			}
		}
	}
	else if (input_size == 4)
	{
        unsigned long symbol_size = 0;
        symbol_size = mynl_4grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_4grams_syllable_reduced_size;
        
		for (int i = 0; i < symbol_size; ++i)
		{
			if (strcmp(lowercased_candidate, mynl_4grams_syllable[i]) == 0)
			{
				found_index = i;
                found_ngram_length=4;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram4;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
				break;
			}
		}
	}
	else if (input_size == 3)
	{
        unsigned long symbol_size = 0;
        symbol_size = mynl_3grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_3grams_syllable_reduced_size;
        
		for (int i = 0; i < symbol_size; ++i)
		{
			if (strcmp(lowercased_candidate, mynl_3grams_syllable[i]) == 0)
			{
				found_index = i;
                found_ngram_length=3;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram3;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
				break;
			}
		}
	}
	else if (input_size == 2)
	{
        unsigned long symbol_size = 0;
        symbol_size = mynl_2grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_2grams_syllable_reduced_size;
        
		for (int i = 0; i < symbol_size; ++i)
		{
			if (strcmp(lowercased_candidate, mynl_2grams_syllable[i]) == 0)
			{
				found_index = i;
                found_ngram_length=2;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram2;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
				break;
			}
		}
	}
    else if (input_size == 1)
    {
        //uncodable n-gram found
        //code every character in ngram based on its original length
		for (unsigned long i = 0; i < original_substring_length; ++i)
		{
			output_found_array[*output_find_count] = input_string[i];
			output_found_category[*output_find_count] = uncoded;
			output_found_capstyle[*output_find_count] = 3;
			(*output_find_count)++;
            *output_remaining_unprocessed_substring_length-=1;
		}
    }

	free(candidate);
	free(lowercased_candidate);

    
    if (found_ngram_length == 0)
    {
        //resize the ngram window by 1 char backward
        next_input_search_start_position+= 1;
    }
    else
    {
        //move forward the ngram window to the remaining part of the word
        unsigned long starting_ngram=5;
        
        if (*output_remaining_unprocessed_substring_length < 5)
            starting_ngram = *output_remaining_unprocessed_substring_length;              
        
        
        if (next_input_search_start_position > starting_ngram)
            next_input_search_start_position-= starting_ngram;
        else
            next_input_search_start_position = 0;
        
        if (next_input_search_end_position > found_ngram_length)
            next_input_search_end_position-= found_ngram_length;
        else
            next_input_search_end_position = 0;
        next_substring_length = next_input_search_end_position - next_input_search_start_position;
    }
    
    if (next_input_search_start_position == next_input_search_end_position)
    {
		//exit clause for recursion
        if (*output_remaining_unprocessed_substring_length >= 1)
        {
            //single char leftover, encode as ASCII
            for (int i = (int)*output_remaining_unprocessed_substring_length - 1; i >= 0; --i)
            {
                output_found_array[*output_find_count] = input_string[i];
                output_found_category[*output_find_count] = uncoded;
                output_found_capstyle[*output_find_count] = get_capitalization_style(input_string, input_size);
                (*output_find_count)++;
                if (verbose_debug_mode==1)
                    printf(" > %c",input_string[i]);
            }
            
            
        }
        if (verbose_debug_mode==1)
            printf("\n");
        return -1;
    }
    else
    {
        return stem_grams(input_string, next_substring_length, next_input_search_start_position, next_input_search_end_position, output_found_array, output_found_category, output_found_capstyle, output_find_count, output_remaining_unprocessed_substring_length);
    }
}

//recursively stem out syllables from input_string in non-destructive way
//output - output_found_array, output_found_category, output_found_capstyle
//          , output_find_count, output_remaining_unprocessed_substring_length
//return - -1 when substring is processed completely
int stem_grams_keyed(const char* input_string, unsigned long original_substring_length, unsigned long input_search_start_position, unsigned long input_search_end_position, int* output_found_array, int* output_found_category, int* output_found_capstyle, unsigned long* output_find_count, unsigned long* output_remaining_unprocessed_substring_length)
{
    //gradually stem out ngrams in a word
    int found_index = -1;
    unsigned long found_ngram_length = 0;
    unsigned long next_input_search_start_position = input_search_start_position;
    unsigned long next_input_search_end_position = input_search_end_position;
    
    //create array of string the length of input string and the size of input
    unsigned long input_size = (input_search_end_position - input_search_start_position)+1;
    unsigned long next_substring_length = input_size;
    
    //sanity check
    if (input_size == 0)
        return -1;
    
    char* candidate = NULL;
    candidate = calloc(input_size+1, sizeof(char));
    memcpy(candidate, input_string + input_search_start_position, input_size+1);
    candidate[input_size] = 0;
    
    //create a lowercased string for lookup against sylable list
    char* lowercased_candidate = NULL;
    lowercased_candidate = calloc(input_size + 1, sizeof(char));
    memcpy(lowercased_candidate, input_string + input_search_start_position, input_size+1);
    for (unsigned long i = 0; i < input_size; ++i)
    {
        lowercased_candidate[i]=tolower(lowercased_candidate[i]);
    }
    lowercased_candidate[input_size] = 0;
    
    if (input_size == 5)
    {
        unsigned long symbol_size = 0;
        symbol_size = mynl_5grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_5grams_syllable_reduced_size;
        
        for (int i = 0; i < symbol_size; ++i)
        {
            if (strcmp(lowercased_candidate, mynl_5grams_syllable[keyed_5grams_code_table[i]]) == 0)
            {
                found_index = i;
                found_ngram_length=5;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram5;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
                break;
            }
        }
    }
    else if (input_size == 4)
    {
        unsigned long symbol_size = 0;
        symbol_size = mynl_4grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_4grams_syllable_reduced_size;
        
        for (int i = 0; i < symbol_size; ++i)
        {
            if (strcmp(lowercased_candidate, mynl_4grams_syllable[keyed_4grams_code_table[i]]) == 0)
            {
                found_index = i;
                found_ngram_length=4;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram4;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
                break;
            }
        }
    }
    else if (input_size == 3)
    {
        unsigned long symbol_size = 0;
        symbol_size = mynl_3grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_3grams_syllable_reduced_size;
        
        for (int i = 0; i < symbol_size; ++i)
        {
            if (strcmp(lowercased_candidate, mynl_3grams_syllable[keyed_3grams_code_table[i]]) == 0)
            {
                found_index = i;
                found_ngram_length=3;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram3;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
                break;
            }
        }
    }
    else if (input_size == 2)
    {
        unsigned long symbol_size = 0;
        symbol_size = mynl_2grams_syllable_size;
        
        //use bitwidth reduction if it is set
        if (lossy_reduce_symbol_width_mode==1)
            symbol_size = mynl_2grams_syllable_reduced_size;
        
        for (int i = 0; i < symbol_size; ++i)
        {
            if (strcmp(lowercased_candidate, mynl_2grams_syllable[keyed_2grams_code_table[i]]) == 0)
            {
                found_index = i;
                found_ngram_length=2;
                output_found_array[*output_find_count] = i;
                output_found_category[*output_find_count] = gram2;
                output_found_capstyle[*output_find_count] = get_capitalization_style(candidate, input_size);
                (*output_find_count)++;
                *output_remaining_unprocessed_substring_length-=found_ngram_length;
                if (verbose_debug_mode==1)
                    printf(" > %s",candidate);
                break;
            }
        }
    }
    else if (input_size == 1)
    {
        //uncodable n-gram found
        //code every character in ngram based on its original length
        for (unsigned long i = 0; i < original_substring_length; ++i)
        {
            for (int j=0; j < 256; ++j)
            {
                if (input_string[i] == keyed_uncoded_code_table[j])
                {
                    output_found_array[*output_find_count] = j;
                    output_found_category[*output_find_count] = uncoded;
                    output_found_capstyle[*output_find_count] = 3;
                    (*output_find_count)++;
                    *output_remaining_unprocessed_substring_length-=1;
                    break;
                }
            }
            
            
        }
    }
    
    free(candidate);
    free(lowercased_candidate);
    
    
    if (found_ngram_length == 0)
    {
        //resize the ngram window by 1 char backward
        next_input_search_start_position+= 1;
    }
    else
    {
        //move forward the ngram window to the remaining part of the word
        unsigned long starting_ngram=5;
        
        if (*output_remaining_unprocessed_substring_length < 5)
            starting_ngram = *output_remaining_unprocessed_substring_length;
        
        
        if (next_input_search_start_position > starting_ngram)
            next_input_search_start_position-= starting_ngram;
        else
            next_input_search_start_position = 0;
        
        if (next_input_search_end_position > found_ngram_length)
            next_input_search_end_position-= found_ngram_length;
        else
            next_input_search_end_position = 0;
        next_substring_length = next_input_search_end_position - next_input_search_start_position;
    }
    
    if (next_input_search_start_position == next_input_search_end_position)
    {
        //exit clause for recursion
        if (*output_remaining_unprocessed_substring_length >= 1)
        {
            //single char leftover, encode as ASCII
            for (int i = (int)*output_remaining_unprocessed_substring_length - 1; i >= 0; --i)
            {
                for (int j=0; j < 256; ++j)
                {
                    if (input_string[i] == keyed_uncoded_code_table[j])
                    {
                        output_found_array[*output_find_count] = j;
                        output_found_category[*output_find_count] = uncoded;
                        output_found_capstyle[*output_find_count] = get_capitalization_style(input_string, input_size);
                        (*output_find_count)++;
                        if (verbose_debug_mode==1)
                            printf(" > %c",input_string[i]);
                        break;
                    }
                }
                
            }
            
            
        }
        if (verbose_debug_mode==1)
            printf("\n");
        return -1;
    }
    else
    {
        return stem_grams_keyed(input_string, next_substring_length, next_input_search_start_position, next_input_search_end_position, output_found_array, output_found_category, output_found_capstyle, output_find_count, output_remaining_unprocessed_substring_length);
    }
}

//Encode the values specified in input_code_array, input_types and input_capstyle arrays into a series of
//bytes specified by output_code
//output - output_code
//return - total char count of encoded data
unsigned long encode_variable_width(int* input_code_array, int* input_types, int* input_capstyle, unsigned long input_size, unsigned char* output_code)
{
	//in variable-width code, each code category has varied width.
    //Here it's between 6 to 13-bit wide, inclusive of 3 bits used for code type header
	//size	meaning
    //==============
	//	11	uncoded - 8
	//	8	5 - gram - 5
	//	12	4 - gram - 9
	//	15	3 - gram - 12
	//	12	2 - gram - 9
	//	13	common words - 10
	//	6	Terminus 1 gram - 3
	//	7	Terminus 2 grams - 4
    //==============
    //Code is written into the bits in the following format
    //[code type header][capitalization style][code value trailer]

	//count number of chars required for output
	unsigned long chars_count = 0;
    unsigned long bits_count = 0;
    
    //calculate maximum width of data based on header type
	for (unsigned long i = 0; i < input_size; ++i)
    {
        int header_code_width = 0;
        int trailer_code_width = 0;
        int capstyle_code_width=0;
        if (input_types[i] == gram5)
        {
            header_code_width = 3;
            trailer_code_width = 3;
            capstyle_code_width=2;
        }
        else if (input_types[i] == gram4)
        {
            header_code_width = 3;
            trailer_code_width = 7;
            capstyle_code_width=2;
        }
        else if (input_types[i] == gram3)
        {
            header_code_width = 3;
            trailer_code_width = 10;
            capstyle_code_width=2;
        }
        else if (input_types[i] == gram2)
        {
            header_code_width = 3;
            trailer_code_width = 7;
            capstyle_code_width=2;
        }
		else if (input_types[i] == commonwords)
		{
			header_code_width = 3;
			trailer_code_width = 8;
			capstyle_code_width = 2;
		}
        else if (input_types[i] == terminal2)
        {
            header_code_width = 3;
            trailer_code_width = 4;
            capstyle_code_width=0;
        }
        else if (input_types[i] == terminal1)
        {
            header_code_width = 3;
            trailer_code_width = 3;
            capstyle_code_width=0;
        }
        else
        {
            //uncoded
            header_code_width = 3;
            capstyle_code_width=0;
            trailer_code_width = 8;
        }
        
        //lossy mode
        //ignore capitalization data if mode is set
        if (lossy_ignore_capitalization_mode==1)
            capstyle_code_width=0;
        
        //use bitwidth reduction if it is set
        //only for n-grams
        if (lossy_reduce_symbol_width_mode==1 && (input_types[i]!=terminal1 && input_types[i] != terminal2))
            trailer_code_width-=1;
        
        int total_code_width = header_code_width + capstyle_code_width + trailer_code_width;
        bits_count+=total_code_width;
    }
    
    int bit_remainder = bits_count % 8;
    if (bit_remainder > 0)
        chars_count = (bits_count / 8)+1;
    else
        chars_count = bits_count/8;
    
    //create character bits array for debug printout purpose
    char* bits_array = calloc(bits_count+1, sizeof(char));
    
	//define char array for output
	unsigned char* code_series = NULL;
	code_series = calloc(chars_count, sizeof(char));

	//define probable size of output based
	//on gram length
	unsigned long string_length = 0;

	//set bit of each char
	int char_bit_counter = 0;
	unsigned long char_array_pos = 0;
    int code_counter=0;
    
	while (char_array_pos < chars_count)
	{
		int header_code_width = 0;
		int trailer_code_width = 0;
        int capstyle_code_width=0;
        //deduce bit size of header and trailer data
		if (input_types[code_counter] == gram5)
		{
			header_code_width = 3;
			trailer_code_width = 3;
            capstyle_code_width=2;
			string_length += gram5;
		}
		else if (input_types[code_counter] == gram4)
		{
			header_code_width = 3;
			trailer_code_width = 7;
            capstyle_code_width=2;
			string_length += gram4;
		}
		else if (input_types[code_counter] == gram3)
		{
			header_code_width = 3;
			trailer_code_width = 10;
            capstyle_code_width=2;
			string_length += gram3;
		}
		else if (input_types[code_counter] == gram2)
		{
			header_code_width = 3;
			trailer_code_width = 7;
            capstyle_code_width=2;
			string_length += gram2;
		}
		else if (input_types[code_counter] == commonwords)
		{
			header_code_width = 3;
			trailer_code_width = 8;
			capstyle_code_width = 2;
			string_length += max_commonwords_length;
		}
		else if (input_types[code_counter] == terminal2)
		{
			header_code_width = 3;
			trailer_code_width = 4;
            capstyle_code_width=0;
			string_length += 2;
		}
		else if (input_types[code_counter] == terminal1)
		{
			header_code_width = 3;
			trailer_code_width = 3;
            capstyle_code_width=0;
			string_length += 1;
		}
		else
		{
			//uncoded
			header_code_width = 3;
			trailer_code_width = 8;
            capstyle_code_width=0;
			string_length += 1;
		}
        
        //lossy mode
        //ignore capitalization data if mode is set
        if (lossy_ignore_capitalization_mode==1)
            capstyle_code_width=0;
        
        //use bitwidth reduction if it is set
        //only for n-grams
        if (lossy_reduce_symbol_width_mode==1 && (input_types[code_counter]!=terminal1 && input_types[code_counter] != terminal2))
            trailer_code_width-=1;
        
        //add header data into code series
        for (int i=0; i < header_code_width; ++i)
        {
            int bitvalue = get_char_bit_value(i, input_types[code_counter]);
            if (bitvalue==1)
                set_char_bit(char_bit_counter, &code_series[char_array_pos]);
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(code_series[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                char_bit_counter=0;
                ++char_array_pos;
            }
        }
        
        //add capitalization style data if available.
        //not put during terminal encoding
        for (int i=0; i < capstyle_code_width; ++i)
        {
            int bitvalue = get_char_bit_value(i, input_capstyle[code_counter]);
            if (bitvalue==1)
                set_char_bit(char_bit_counter, &code_series[char_array_pos]);
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(code_series[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                char_bit_counter=0;
                ++char_array_pos;
            }
        }

        
        //add trailer data for coded n-gram index
        for (int i=0; i < trailer_code_width; ++i)
        {
            int bitvalue = 0;
            if (trailer_code_width < 8)
                bitvalue = get_char_bit_value(i, input_code_array[code_counter]);
            else
                bitvalue = get_int_bit_value(i, input_code_array[code_counter]);
            
            if (bitvalue==1)
                set_char_bit(char_bit_counter, &code_series[char_array_pos]);
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(code_series[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                char_bit_counter=0;
                ++char_array_pos;
            }
        }
        
        ++code_counter;
        if (code_counter==input_size)
            break;
	}
    
    //copy to output
    if (output_code!=NULL)
        memcpy(output_code, code_series, sizeof(unsigned char)*chars_count);
    
    //release
    free(bits_array);
    free(code_series);
    return chars_count;
}

//Parse the code data input_string to find series of code type headers, code values and code capitalization flag
//output - output_code_type_array, output_code_value_array, output_code_capstyle_array, output_code_size
//return - predicted total character count of encoded string
unsigned long decode_variable_width(const unsigned char* input_string, unsigned long input_size, int* output_code_type_array, int* output_code_value_array, int* output_code_capstyle_array, unsigned long* output_code_size)
{
    unsigned long output_size=0;
    unsigned long chars_count = 0;
    unsigned long bits_count = 0;
    
    //because bits is written from the back during encode
    //it should now be read from the front
    int char_bit_counter = 0;
	unsigned long char_array_pos = 0;
    float int_array_pos=0;
    int code_counter=0;
    int processed_code_width=0;    
    
    chars_count = input_size;
    bits_count = chars_count * 8;
    
    //create character bits array for debug printout purpose
    char* bits_array = calloc(bits_count+1, sizeof(char));
    
    //parse code from front
    //get 3-bit header to deduce the code category
    while (char_array_pos < chars_count)
    {
        int header_code_width = 0;
        int trailer_code_width = 0;
        int capstyle_code_width = 0;
        //header is always 3-bits
        header_code_width = 3;
        
        //get 3-bit header to deduce the code category
        //and get the trailer code width
        unsigned char header_type=0;
        unsigned char capstyle_type=0;
        unsigned int trailer_type_int=0;
        for (int i=0; i < header_code_width; ++i)
        {
            int bitvalue = get_char_bit_value(char_bit_counter, input_string[char_array_pos]);
            if (bitvalue==1)
                set_char_bit(i, &header_type);
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(input_string[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                
                char_bit_counter=0;
                ++char_array_pos;
            }
        }
        
        //add into array
        output_code_type_array[code_counter]=header_type;
		//mark the bits segment parsed so far as processed
        processed_code_width+=header_code_width;
        
        //deduce type of code
        if (header_type == gram5)
        {
            trailer_code_width = 3;
            capstyle_code_width=2;
            output_size+= gram5;
        }
        else if (header_type == gram4)
        {
            trailer_code_width = 7;
            capstyle_code_width=2;
            output_size += gram4;
        }
        else if (header_type == gram3)
        {
            trailer_code_width = 10;
            capstyle_code_width=2;
            output_size += gram3;
        }
        else if (header_type == gram2)
        {
            trailer_code_width = 7;
            capstyle_code_width=2;
            output_size += gram2;
        }
		else if (header_type == commonwords)
		{
			trailer_code_width = 8;
			capstyle_code_width = 2;
			output_size += max_commonwords_length;
		}
        else if (header_type == terminal2)
        {
            trailer_code_width = 4;
            output_size+=2;
        }
        else if (header_type == terminal1)
        {
            trailer_code_width = 3;
            output_size+=1;
        }
        else
        {
            //uncoded
            trailer_code_width = 8;
            output_size+=1;
        }
        
        //lossy mode
        //ignore capitalization data if mode is set
        if (lossy_ignore_capitalization_mode==1)
            capstyle_code_width=0;
        
        //use bitwidth reduction if it is set
        //only for n-grams
        if (lossy_reduce_symbol_width_mode==1 && (header_type !=terminal1 && header_type != terminal2))
            trailer_code_width-=1;
        
        //get capstyle if available
        for (int i=0; i < capstyle_code_width; ++i)
        {
            int bitvalue = get_char_bit_value(char_bit_counter, input_string[char_array_pos]);
            if (bitvalue==1)
                set_char_bit(i, &capstyle_type);
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(input_string[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                char_bit_counter=0;
                ++char_array_pos;
            }
        }
        
        //mark the bits segment parsed so far as processed
        //set capstyle, or default to 3 (all lowercase) if not available
        if (capstyle_code_width != 0)
            output_code_capstyle_array[code_counter]=capstyle_type;
        else
            output_code_capstyle_array[code_counter]=3;
        processed_code_width+=capstyle_code_width;

        
        //get trailer data to deduce index of n-gram		
        for (int i=0; i < trailer_code_width; ++i)
        {
            int bitvalue = get_char_bit_value(char_bit_counter, input_string[char_array_pos]);
            
            if (bitvalue==1)
                set_int_bit(i, &trailer_type_int);
            
            ++char_bit_counter;
            if (char_bit_counter > 7)
            {
                if (verbose_debug_mode==1)
                {
                    int bits[8];
                    convert_char_to_bit_array(input_string[char_array_pos], &bits[0]);
                    for (int i=0; i < 8; ++i)
                    {
                        if (bits[i]==0)
                            bits_array[((char_array_pos*8)+i)]='0';
                        else
                            bits_array[((char_array_pos*8)+i)]='1';
                    }
                    bits_array[bits_count]=0;
                    printf("%s\n",bits_array);
                }
                char_bit_counter=0;
                ++char_array_pos;
                int_array_pos=int_array_pos+(sizeof(char) / sizeof(int));
                
                
            }
        }        
        
		//add into array
		output_code_value_array[code_counter] = trailer_type_int;
        //mark the bits segment parsed so far as processed
        processed_code_width+=trailer_code_width;
        
        //get size
        
        
        ++code_counter;
        if (code_counter==input_size-1)
            break;
    }
    
    free(bits_array);
	*output_code_size = code_counter;
    return output_size;
}

//Lookup the values specified in input_code_value_array by their respective type in input_code_type_array,
//to find a string with matching index value in the code table and recapitalize them using settings
//in input_code_capstyle_array.
//output - output_string, output_max_size
//return - total characters of the generated string
unsigned long generate_word(int* input_code_type_array, int* input_code_value_array, int* input_code_capstyle_array, unsigned long input_size, char* output_string, unsigned long* output_max_size)
{

	unsigned long char_count = 0;
    int current_word_start_position = 0;
    int current_word_end_position = 0;
	for (unsigned long i = 0; i < input_size; ++i)
	{
		int header_type = input_code_type_array[i];
		int code_value = input_code_value_array[i];
        int caps_style = input_code_capstyle_array[i];
        
		if (header_type == gram5)
		{
			memcpy(output_string + char_count, mynl_5grams_syllable[code_value],gram5);
            set_capitalization_style(caps_style, gram5, &output_string[char_count]);
			char_count += gram5;
            current_word_end_position += gram5;
		}
		else if (header_type == gram4)
		{			
			memcpy(output_string + char_count, mynl_4grams_syllable[code_value], gram4);
            set_capitalization_style(caps_style, gram4, &output_string[char_count]);
			char_count += gram4;
            current_word_end_position += gram4;
		}
		else if (header_type == gram3)
		{			
			memcpy(output_string + char_count, mynl_3grams_syllable[code_value], gram3);
            set_capitalization_style(caps_style, gram3, &output_string[char_count]);
			char_count += gram3;
            current_word_end_position += gram3;
		}
		else if (header_type == gram2)
		{			
			memcpy(output_string + char_count, mynl_2grams_syllable[code_value], gram2);
            set_capitalization_style(caps_style, gram2, &output_string[char_count]);
			char_count += gram2;
            current_word_end_position += gram2;
		}
		else if (header_type == commonwords)
		{
			memcpy(output_string + char_count, mynl_commons[code_value], strlen(mynl_commons[code_value]));
			set_capitalization_style(caps_style, gram2, &output_string[char_count]);
			char_count += strlen(mynl_commons[code_value]);
			current_word_end_position += strlen(mynl_commons[code_value]);
		}
		else if (header_type == terminal2)
		{			
			memcpy(output_string + char_count, mynl_2grams_terminals[code_value], 2);
			char_count += 2;
            //unsigned long current_word_length = abs(current_word_end_position - current_word_start_position);
            //set_capitalization_style(caps_style, current_word_length, &output_string[0]);
            current_word_start_position = current_word_end_position;
            current_word_end_position = 0;
		}
		else if (header_type == terminal1)
		{			
			memcpy(output_string + char_count, mynl_1grams_terminals[code_value], 1);
			char_count += 1;
            //unsigned long current_word_length = abs(current_word_end_position - current_word_start_position);
            //set_capitalization_style(caps_style, current_word_length, &output_string[0]);
            current_word_start_position = current_word_end_position;
            current_word_end_position = 0;
		}
		else
		{
			//uncoded
            unsigned char bytes[4];
            split_int_to_chars(code_value, &bytes[0]);
			//put code value directly as it is ASCII data
			memcpy(output_string + char_count, &bytes[3], 1);
			char_count += 1;
		}
	}

	return char_count;
}

//Lookup the values specified in input_code_value_array by their respective type in input_code_type_array,
//to find a string with matching index value in the code table and recapitalize them using settings
//in input_code_capstyle_array.
//output - output_string, output_max_size
//return - total characters of the generated string
unsigned long generate_word_keyed(int* input_code_type_array, int* input_code_value_array, int* input_code_capstyle_array, unsigned long input_size, char* output_string, unsigned long* output_max_size)
{
    
    unsigned long char_count = 0;
    int current_word_start_position = 0;
    int current_word_end_position = 0;
    for (unsigned long i = 0; i < input_size; ++i)
    {
        int header_type = input_code_type_array[i];
        int code_value = input_code_value_array[i];
        int caps_style = input_code_capstyle_array[i];
        
        if (header_type == gram5)
        {
            memcpy(output_string + char_count, mynl_5grams_syllable[keyed_5grams_code_table[code_value]],gram5);
            set_capitalization_style(caps_style, gram5, &output_string[char_count]);
            char_count += gram5;
            current_word_end_position += gram5;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_5grams_syllable[keyed_5grams_code_table[code_value]]);
            }
        }
        else if (header_type == gram4)
        {
            memcpy(output_string + char_count, mynl_4grams_syllable[keyed_4grams_code_table[code_value]], gram4);
            set_capitalization_style(caps_style, gram4, &output_string[char_count]);
            char_count += gram4;
            current_word_end_position += gram4;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_4grams_syllable[keyed_4grams_code_table[code_value]]);
            }
        }
        else if (header_type == gram3)
        {
            memcpy(output_string + char_count, mynl_3grams_syllable[keyed_3grams_code_table[code_value]], gram3);
            set_capitalization_style(caps_style, gram3, &output_string[char_count]);
            char_count += gram3;
            current_word_end_position += gram3;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_3grams_syllable[keyed_3grams_code_table[code_value]]);
            }
        }
        else if (header_type == gram2)
        {
            memcpy(output_string + char_count, mynl_2grams_syllable[keyed_2grams_code_table[code_value]], gram2);
            set_capitalization_style(caps_style, gram2, &output_string[char_count]);
            char_count += gram2;
            current_word_end_position += gram2;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_2grams_syllable[keyed_2grams_code_table[code_value]]);
            }
        }
        else if (header_type == commonwords)
        {
            memcpy(output_string + char_count, mynl_commons[keyed_commons_code_table[code_value]], strlen(mynl_commons[keyed_commons_code_table[code_value]]));
            set_capitalization_style(caps_style, gram2, &output_string[char_count]);
            char_count += strlen(mynl_commons[keyed_commons_code_table[code_value]]);
            current_word_end_position += strlen(mynl_commons[keyed_commons_code_table[code_value]]);
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_commons[keyed_commons_code_table[code_value]]);
            }
        }
        else if (header_type == terminal2)
        {
            memcpy(output_string + char_count, mynl_2grams_terminals[keyed_2terminal_code_table[code_value]], 2);
            char_count += 2;
            //unsigned long current_word_length = abs(current_word_end_position - current_word_start_position);
            //set_capitalization_style(caps_style, current_word_length, &output_string[0]);
            current_word_start_position = current_word_end_position;
            current_word_end_position = 0;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_2grams_terminals[keyed_2terminal_code_table[code_value]]);
            }
        }
        else if (header_type == terminal1)
        {
            memcpy(output_string + char_count, mynl_1grams_terminals[keyed_1terminal_code_table[code_value]], 1);
            char_count += 1;
            //unsigned long current_word_length = abs(current_word_end_position - current_word_start_position);
            //set_capitalization_style(caps_style, current_word_length, &output_string[0]);
            current_word_start_position = current_word_end_position;
            current_word_end_position = 0;
            if (verbose_debug_mode==1)
            {
                printf(" > %s ",mynl_1grams_terminals[keyed_1terminal_code_table[code_value]]);
            }
        }
        else
        {
            //uncoded
            unsigned char bytes[4];
            split_int_to_chars(code_value, &bytes[0]);
            //put code value directly as it is ASCII data
            unsigned char value=0;
            for (int i=0; i < 256; ++i)
            {
                if (bytes[3]==keyed_uncoded_code_table[i])
                {
                    value=keyed_uncoded_code_table[bytes[3]];
                }
            }
            memcpy(output_string + char_count, &value, 1);
            char_count += 1;
            if (verbose_debug_mode==1)
            {
                printf(" > %c ",value);
            }
        }
    }
    
    return char_count;
}

//Commence stemming process by extracting individual syllable starting at string pointer
//p_substring_start_position
//output - output_codes, output_types, output_capstyle
//return - total codable n-grams found in current word
unsigned long stem_word(const char* p_substring_start_position, int substring_length, unsigned long current_code_count, int* output_codes, int* output_types, int* output_capstyle)
{
	//define 20 possible results per word
	//because fully-affixed Malay words rarely has more than 50 syllables or chars
    //longest Malay word on record is 'menyetidaknyahcasdiversifikasielektrostatikkan'
	int found_indices[50];
	int found_categories[50];
    int found_cap_style[50];
	unsigned long found_count = 0;
	for (int i = 0; i < 50; ++i)
	{
		found_indices[i] = -1;
        found_categories[i]=-1;
        found_cap_style[i]=-1;
	}
		
	//define initial start
	int starting_gram = 5;
    if (substring_length < starting_gram)
        starting_gram = substring_length;
    int substring_start_position = substring_length - starting_gram;
	if (substring_start_position < 0)
		substring_start_position = 0;
	int current_search_position = substring_length - 1;
    unsigned long remaining_substring_left_to_process = substring_length;
    
    if (verbose_debug_mode==1)
        printf("Stemming started.\n");

	if (extract_common_words(p_substring_start_position, substring_length, &found_indices[0], &found_categories[0], &found_cap_style[0], &found_count) == -1)
	{
		//try to stem largest syllables first, from the back
		stem_grams(p_substring_start_position, starting_gram, substring_start_position, current_search_position, &found_indices[0], &found_categories[0], &found_cap_style[0], &found_count, &remaining_substring_left_to_process);
	}	
	
    //copy to output and reorder to the front
	for (unsigned long i = 0; i < found_count; ++i)
    {
        output_codes[current_code_count + i] = found_indices[found_count - 1 - i];
        output_types[current_code_count + i] = found_categories[found_count - 1 - i];
        output_capstyle[current_code_count + i] = found_cap_style[found_count - 1 - i];
    }
    
    if (verbose_debug_mode==1)
        printf("Stemming ended, %ld codes generated\n", found_count);

    return found_count;
}

//Commence stemming process by extracting individual syllable starting at string pointer
//p_substring_start_position
//output - output_codes, output_types, output_capstyle
//return - total codable n-grams found in current word
unsigned long stem_word_keyed(const char* p_substring_start_position, int substring_length, unsigned long current_code_count, int* output_codes, int* output_types, int* output_capstyle)
{
    //define 20 possible results per word
    //because fully-affixed Malay words rarely has more than 50 syllables or chars
    //longest Malay word on record is 'menyetidaknyahcasdiversifikasielektrostatikkan'
    int found_indices[50];
    int found_categories[50];
    int found_cap_style[50];
    unsigned long found_count = 0;
    for (int i = 0; i < 50; ++i)
    {
        found_indices[i] = -1;
        found_categories[i]=-1;
        found_cap_style[i]=-1;
    }
    
    //define initial start
    int starting_gram = 5;
    if (substring_length < starting_gram)
        starting_gram = substring_length;
    int substring_start_position = substring_length - starting_gram;
    if (substring_start_position < 0)
        substring_start_position = 0;
    int current_search_position = substring_length - 1;
    unsigned long remaining_substring_left_to_process = substring_length;
    
    if (verbose_debug_mode==1)
        printf("Stemming started.\n");
    
    if (extract_common_words_keyed(p_substring_start_position, substring_length, &found_indices[0], &found_categories[0], &found_cap_style[0], &found_count) == -1)
    {
        //try to stem largest syllables first, from the back
        stem_grams_keyed(p_substring_start_position, starting_gram, substring_start_position, current_search_position, &found_indices[0], &found_categories[0], &found_cap_style[0], &found_count, &remaining_substring_left_to_process);
    }
    
    //copy to output and reorder to the front
    for (unsigned long i = 0; i < found_count; ++i)
    {
        output_codes[current_code_count + i] = found_indices[found_count - 1 - i];
        output_types[current_code_count + i] = found_categories[found_count - 1 - i];
        output_capstyle[current_code_count + i] = found_cap_style[found_count - 1 - i];
    }
    
    if (verbose_debug_mode==1)
        printf("Stemming ended, %ld codes generated\n", found_count);
    
    return found_count;
}


//Extract individual word from input_string by finding a terminal symbol and getting preceding
//characters as a single word
//output - output_code_values, output_code_types, output_code_capstyle
//return - total codable n-grams found in entire string
unsigned long parse_words(const char* input_string, unsigned long input_size, int* output_code_values, int* output_code_types, int* output_code_capstyle)
{
    //establish first starting point
    int current_substring_start_pos = 0;
	int current_substring_search_pos = 0;
    
    //Accumulate count of codable data found here
    unsigned long code_found_count = 0;
    
    //try with 2-grams terminals first
    while (current_substring_search_pos < input_size)
    {
        //get count of word found so far
        unsigned long word_found=0;
        //get a window of two chars to check the 2-gram terminal
        char terminal_candidate[2];
        terminal_candidate[0]= input_string[current_substring_search_pos];
        terminal_candidate[1]=input_string[current_substring_search_pos+1];
        int terminal_2gram_found=0;
        int terminal_1gram_found=0;
        
        //search for 1-gram terminal first
		for (int i = 0; i < mynl_1grams_terminals_size; ++i)
        {
            
            //when matching terminal is found, try comparing with the 2-gram terminals first
            if (terminal_candidate[0]==mynl_1grams_terminals[i][0])
            {
				for (int j = 0; j < mynl_2grams_terminals_size; ++j)
                {
                    if (terminal_candidate[0]==mynl_2grams_terminals[j][0] && terminal_candidate[1] == mynl_2grams_terminals[j][1])
                    {
                        //get index range between last search start position and current search position
                        //and start stemming
                        int word_length = current_substring_search_pos - current_substring_start_pos;
                        unsigned long codable_count = 0;
                        char* word_string = NULL;
                        word_string = calloc(word_length+1, sizeof(char));
                        memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
                        word_string[word_length] = 0;
                        
                        if (verbose_debug_mode==1)
                            printf("Terminus found. ");
                        
                        codable_count = stem_word(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
                        
                        //add total code found to counter
                        code_found_count+= codable_count;
                        
                        //add terminal code to result array
                        output_code_values[code_found_count]=j;
                        output_code_types[code_found_count]=terminal2;
                        output_code_capstyle[code_found_count]=0;
                        ++code_found_count;
                        ++word_found;
                        free(word_string);
                        ++terminal_2gram_found;
                        current_substring_start_pos=current_substring_search_pos+2;
                        break;
                    }
                }
                
                //break since a terminal has been found already
                if (terminal_2gram_found>0)
                    break;
                else
                {
                    //process as 1-gram terminal
                    //get index range between last search start position and current search position
                    //and start stemming
                    int word_length = current_substring_search_pos - current_substring_start_pos;
                    unsigned long codable_count = 0;
                    char* word_string = NULL;
                    word_string = calloc(word_length+1, sizeof(char));
                    memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
                    word_string[word_length] = 0;
                    
                    if (verbose_debug_mode==1)
                        printf("Terminus found. ");
                    
                    codable_count = stem_word(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
                    
                    //add total code found to counter
                    code_found_count+= codable_count;
                    
                    //add terminal code to result array
                    output_code_values[code_found_count]=i;
                    output_code_types[code_found_count]=terminal1;
                    output_code_capstyle[code_found_count]=0;
                    ++code_found_count;
                    ++word_found;
                    free(word_string);
                    ++terminal_1gram_found;
                    current_substring_start_pos=current_substring_search_pos+1;
                    break;
                }
            }
        }
        
        if (terminal_1gram_found == 0 && terminal_2gram_found == 0 && (current_substring_search_pos + 1) == input_size && (current_substring_search_pos-current_substring_start_pos)>0)
        {
            //near end of string, but some string left unprocessed
            //get the text until the end of string
            //get index range between last search start position and current search position + null terminator
            //and start stemming
            int word_length = (current_substring_search_pos - current_substring_start_pos)+1;
            unsigned long codable_count = 0;
            char* word_string = NULL;
            word_string = calloc(word_length+1, sizeof(char));
            memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
            word_string[word_length] = 0;
            
            if (verbose_debug_mode==1)
                printf("Unterminated string found. ");
            
            codable_count = stem_word(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
            
            //add total code found to counter
            code_found_count+= codable_count;
            
            free(word_string);
            current_substring_start_pos=current_substring_search_pos;
            break;

        }
        
        
        if (terminal_2gram_found>0)
            current_substring_search_pos+=2;
        else
            ++current_substring_search_pos;
        
        
    }
    
    return code_found_count;
    
}

//stem out string using the permuted tables
unsigned long parse_words_keyed(const char* input_string, unsigned long input_size, int* output_code_values, int* output_code_types, int* output_code_capstyle)
{
    //establish first starting point
    int current_substring_start_pos = 0;
    int current_substring_search_pos = 0;
    
    //Accumulate count of codable data found here
    unsigned long code_found_count = 0;
    
    //try with 2-grams terminals first
    while (current_substring_search_pos < input_size)
    {
        //get count of word found so far
        unsigned long word_found=0;
        //get a window of two chars to check the 2-gram terminal
        char terminal_candidate[2];
        terminal_candidate[0]= input_string[current_substring_search_pos];
        terminal_candidate[1]=input_string[current_substring_search_pos+1];
        int terminal_2gram_found=0;
        int terminal_1gram_found=0;
        
        //search for 1-gram terminal first
        for (int i = 0; i < mynl_1grams_terminals_size; ++i)
        {
            
            //when matching terminal is found, try comparing with the 2-gram terminals first
            if (terminal_candidate[0]==mynl_1grams_terminals[keyed_1terminal_code_table[i]][0])
            {
                for (int j = 0; j < mynl_2grams_terminals_size; ++j)
                {
                    if (terminal_candidate[0]==mynl_2grams_terminals[keyed_2terminal_code_table[j]][0] && terminal_candidate[1] == mynl_2grams_terminals[keyed_2terminal_code_table[j]][1])
                    {
                        //get index range between last search start position and current search position
                        //and start stemming
                        int word_length = current_substring_search_pos - current_substring_start_pos;
                        unsigned long codable_count = 0;
                        char* word_string = NULL;
                        word_string = calloc(word_length+1, sizeof(char));
                        memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
                        word_string[word_length] = 0;
                        
                        if (verbose_debug_mode==1)
                            printf("Terminus found. ");
                        
                        codable_count = stem_word_keyed(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
                        
                        //add total code found to counter
                        code_found_count+= codable_count;
                        
                        //add terminal code to result array
                        output_code_values[code_found_count]=j;
                        output_code_types[code_found_count]=terminal2;
                        output_code_capstyle[code_found_count]=0;
                        ++code_found_count;
                        ++word_found;
                        free(word_string);
                        ++terminal_2gram_found;
                        current_substring_start_pos=current_substring_search_pos+2;
                        break;
                    }
                }
                
                //break since a terminal has been found already
                if (terminal_2gram_found>0)
                    break;
                else
                {
                    //process as 1-gram terminal
                    //get index range between last search start position and current search position
                    //and start stemming
                    int word_length = current_substring_search_pos - current_substring_start_pos;
                    unsigned long codable_count = 0;
                    char* word_string = NULL;
                    word_string = calloc(word_length+1, sizeof(char));
                    memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
                    word_string[word_length] = 0;
                    
                    if (verbose_debug_mode==1)
                        printf("Terminus found. ");
                    
                    codable_count = stem_word_keyed(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
                    
                    //add total code found to counter
                    code_found_count+= codable_count;
                    
                    //add terminal code to result array
                    output_code_values[code_found_count]=i;
                    output_code_types[code_found_count]=terminal1;
                    output_code_capstyle[code_found_count]=0;
                    ++code_found_count;
                    ++word_found;
                    free(word_string);
                    ++terminal_1gram_found;
                    current_substring_start_pos=current_substring_search_pos+1;
                    break;
                }
            }
        }
        
        if (terminal_1gram_found == 0 && terminal_2gram_found == 0 && (current_substring_search_pos + 1) == input_size && (current_substring_search_pos-current_substring_start_pos)>0)
        {
            //near end of string, but some string left unprocessed
            //get the text until the end of string
            //get index range between last search start position and current search position + null terminator
            //and start stemming
            int word_length = (current_substring_search_pos - current_substring_start_pos)+1;
            unsigned long codable_count = 0;
            char* word_string = NULL;
            word_string = calloc(word_length+1, sizeof(char));
            memcpy(word_string, input_string + current_substring_start_pos, word_length+1);
            word_string[word_length] = 0;
            
            if (verbose_debug_mode==1)
                printf("Unterminated string found. ");
            
            codable_count = stem_word_keyed(word_string, word_length, code_found_count, output_code_values, output_code_types, output_code_capstyle);
            
            //add total code found to counter
            code_found_count+= codable_count;
            
            free(word_string);
            current_substring_start_pos=current_substring_search_pos;
            break;
            
        }
        
        
        if (terminal_2gram_found>0)
            current_substring_search_pos+=2;
        else
            ++current_substring_search_pos;
        
        
    }
    
    return code_found_count;
    
}

#pragma mark public functions
void create_variable_width_code_table(unsigned long long key_1, unsigned long long key_2)
{
    //listing each table separately
    //will create smaller keyspace
    //but increases compression rate
    //due to smaller code lengths
    //and has the possibility of
    //of creating false messages
    //during brute force
    
    //allocate private table for each category
    unsigned long size_5gram = 0;
    unsigned long size_4gram = 0;
    unsigned long size_3gram = 0;
    unsigned long size_2gram = 0;
    unsigned long size_2term = 0;
    unsigned long size_1term = 0;
    unsigned long size_common = 0;
    
    if (lossy_reduce_symbol_width_mode==0)
        size_5gram = mynl_5grams_syllable_size;
    else
        size_5gram = mynl_5grams_syllable_reduced_size;
    
    keyed_5grams_code_table = calloc(size_5gram, sizeof(unsigned int));
    
    if (lossy_reduce_symbol_width_mode==0)
        size_4gram = mynl_4grams_syllable_size;
    else
        size_4gram = mynl_4grams_syllable_reduced_size;
    
    keyed_4grams_code_table = calloc(size_4gram, sizeof(unsigned int));
    
    if (lossy_reduce_symbol_width_mode==0)
        size_3gram = mynl_3grams_syllable_size;
    else
        size_3gram = mynl_3grams_syllable_reduced_size;
    
    keyed_3grams_code_table = calloc(size_3gram, sizeof(unsigned int));
    
    if (lossy_reduce_symbol_width_mode==0)
        size_2gram = mynl_2grams_syllable_size;
    else
        size_2gram = mynl_2grams_syllable_reduced_size;
    
    keyed_3grams_code_table = calloc(size_3gram, sizeof(unsigned int));
    
    if (lossy_reduce_symbol_width_mode==0)
        size_2gram = mynl_2grams_syllable_size;
    else
        size_2gram = mynl_2grams_syllable_reduced_size;
    
    keyed_2grams_code_table = calloc(size_2gram, sizeof(unsigned int));
    
    if (lossy_reduce_symbol_width_mode==0)
        size_common = mynl_commons_size;
    else
        size_common = mynl_commons_reduced_size;
    
    keyed_commons_code_table = calloc(size_common, sizeof(int));
    
    size_2term = mynl_2grams_terminals_size;
    keyed_2terminal_code_table = calloc(size_2term, sizeof(unsigned int));
    
    size_1term = mynl_1grams_terminals_size;
    keyed_1terminal_code_table = calloc(size_1term, sizeof(unsigned int));
    
    keyed_uncoded_code_table = calloc(256, sizeof(unsigned int));
    
    //permute the tables using 2KDP
    //create a table with each data uniquely and randomly being between 0 and keyed_table_size-1
    generate_2kdp_permutation((int)size_5gram, &keyed_5grams_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_4gram, &keyed_4grams_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_3gram, &keyed_3grams_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_2gram, &keyed_2grams_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_2term, &keyed_2terminal_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_1term, &keyed_1terminal_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)size_common, &keyed_commons_code_table[0], key_1, key_2);
    generate_2kdp_permutation((int)256, &keyed_uncoded_code_table[0], key_1, key_2);
}


//map the disparate n-gram arrays to a single
//code table to enable keyed permutation mode
void create_fixed_width_code_table(unsigned long long key_1, unsigned long long key_2)
{
    //listing all the tables as a single table
    //will create a much bigger keyspace
    //compared to separate tables
    
    //region order
    //common word
    //5-gram
    //4-gram
    //3-gram
    //2-gram
    //term-1
    //term-2
    
    //for example
    //[0] 5-gram --> [0] single table --> [4] randomized table
    //[1] 5-gram --> [1] single table --> [1] randomized table
    //[0] 4-gram --> [2] single table --> [78] randomized table
    //[1] 4-gram --> [3] single table --> [0] randomized table
    
    unsigned long keyed_table_size = get_code_table_size();
    keyed_table = calloc(keyed_table_size, sizeof(nl_code_table));
    int* permuted_table = NULL;
    permuted_table = calloc(keyed_table_size, sizeof(int));
    
    
    
    //create a table with each data uniquely and randomly being between 0 and keyed_table_size-1
    generate_2kdp_permutation((int)keyed_table_size, &permuted_table[0], key_1, key_2);
    
    //start by adding in common words first
    int original_index_counter=0;
    for (int i=0 ; i < mynl_commons_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = commonwords;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_5grams_syllable_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = gram5;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_4grams_syllable_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = gram4;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_3grams_syllable_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = gram3;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_2grams_syllable_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = gram2;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_2grams_terminals_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = terminal2;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0 ; i < mynl_1grams_terminals_size; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = terminal1;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    for (int i=0; i < 256; ++i)
    {
        nl_code_table current_code;
        current_code.original_index=original_index_counter;
        current_code.type = uncoded;
        current_code.code = i;
        keyed_table[permuted_table[original_index_counter]]=current_code;
        ++original_index_counter;
    }
    
    free(permuted_table);
}

void free_code_table()
{
    if (keyed_table != NULL)
        free(keyed_table);
    keyed_table = NULL;
    
    if (keyed_5grams_code_table != NULL)
        free(keyed_5grams_code_table);
    keyed_5grams_code_table=NULL;
    if (keyed_4grams_code_table != NULL)
        free(keyed_4grams_code_table);
    keyed_4grams_code_table=NULL;
    if (keyed_3grams_code_table != NULL)
        free(keyed_3grams_code_table);
    keyed_2grams_code_table=NULL;
    if (keyed_2grams_code_table != NULL)
        free(keyed_2grams_code_table);
    keyed_2grams_code_table=NULL;
    if (keyed_2terminal_code_table != NULL)
        free(keyed_2terminal_code_table);
    keyed_2terminal_code_table=NULL;
    if (keyed_1terminal_code_table != NULL)
        free(keyed_1terminal_code_table);
    keyed_1terminal_code_table=NULL;
    if (keyed_commons_code_table != NULL)
        free(keyed_commons_code_table);
    keyed_commons_code_table=NULL;
    if (keyed_uncoded_code_table != NULL)
        free(keyed_uncoded_code_table);
    keyed_uncoded_code_table=NULL;
}


//Returns size of n-gram code table
unsigned long get_code_table_size()
{
    return (mynl_commons_size + mynl_2grams_syllable_size + mynl_3grams_syllable_size + mynl_4grams_syllable_size + mynl_5grams_syllable_size + mynl_1grams_terminals_size + mynl_2grams_terminals_size + 256);
}

//Toggle verbosity mode to enable printout of the processes in the algorithm
void set_verbose()
{
    if (verbose_debug_mode==0)
        verbose_debug_mode=1;
    else if (verbose_debug_mode==1)
        verbose_debug_mode=0;
}

//Toggle option to increase compression rate by ignoring alphabet capitalization
void toggle_lossy_ignore_capitalization()
{
    if (lossy_ignore_capitalization_mode==0)
        lossy_ignore_capitalization_mode=1;
    else if (lossy_ignore_capitalization_mode==1)
        lossy_ignore_capitalization_mode=0;
}

//Toggle option to increase compression rate by ignoring alphabet capitalization
void toggle_lossy_reduce_symbol_width()
{
    if (lossy_reduce_symbol_width_mode==0)
        lossy_reduce_symbol_width_mode=1;
    else if (lossy_reduce_symbol_width_mode==1)
        lossy_reduce_symbol_width_mode=0;
}

//Toggle option to use permuted version of code tables
void toggle_keyed_symbols()
{
    if (keyed_symbols_mode==0)
        keyed_symbols_mode=1;
    else
        keyed_symbols_mode=0;
}

//Compress raw input_string non-destructively
//output - compress_size, error
//return - compressed encoded data
unsigned char* compress_mynl(const char* input_string, unsigned long input_size, unsigned long* compress_size, unsigned long* error)
{
    //results array
    //allocate based on assumption that maximum possible result is equal to sum of chars in string
    int* code_values = NULL;
    int* code_types = NULL;
    int* code_capstyle = NULL;
    unsigned long code_found_count = 0;
    code_values = calloc(input_size, sizeof(int));
    code_types = calloc(input_size, sizeof(int));
    code_capstyle = calloc(input_size, sizeof(int));
    
    if (keyed_symbols_mode==0)
        code_found_count = parse_words(input_string, input_size, &code_values[0], &code_types[0], &code_capstyle[0]);
    else
        code_found_count = parse_words_keyed(input_string, input_size, &code_values[0], &code_types[0], &code_capstyle[0]);
    
    //allocate output twice the size of input for safety
    //actual count is returned in output_size
    unsigned char* output_code = calloc(code_found_count*2, sizeof(unsigned char));
    unsigned long output_size=0;
    output_size = encode_variable_width(&code_values[0], &code_types[0], &code_capstyle[0], code_found_count, &output_code[0]);
    
    free(code_values);
    free(code_types);
    free(code_capstyle);
    if (output_size==0)
    {
        free(output_code);
        return NULL;
    }
    else
    {
        *compress_size = output_size;
        return output_code;
    }
}

//Decompress encoded data input_string non-destructively
//output - decompress_size, error
//return - original uncompressed string
char* decompress_mynl(const unsigned char* input_string, unsigned long input_size, unsigned long* decompress_size, unsigned long* error)
{
    char* output_string = NULL;
	unsigned long output_code_size = 0;
	unsigned long output_string_size = 0;
    //create an output array triple the size of input
    //to anticipate at least 25% compression
    int* code_type_array = calloc(input_size*3, sizeof(int));
    int* code_value_array = calloc(input_size*3, sizeof(int));
    int* code_capstyle_array = calloc(input_size*3, sizeof(int));

    output_string_size = decode_variable_width(input_string, input_size, &code_type_array[0], &code_value_array[0], &code_capstyle_array[0], &output_code_size);
	
	output_string = calloc(output_string_size+1, sizeof(char));
    
    if (keyed_symbols_mode==0)
        *decompress_size = generate_word(&code_type_array[0], &code_value_array[0], &code_capstyle_array[0], output_code_size, &output_string[0], &output_string_size);
    else
        *decompress_size = generate_word_keyed(&code_type_array[0], &code_value_array[0], &code_capstyle_array[0], output_code_size, &output_string[0], &output_string_size);
	
    free(code_capstyle_array);
    free(code_value_array);
    free(code_type_array);
    return output_string;
}

//compress text in file at input_filepath and write to file at output_filepath
unsigned long compress_mynl_file(const char* input_filepath, const char* output_filepath)
{
    char* input_string = NULL;
    unsigned char* output_string = NULL;
    unsigned long compress_size = 0;
    unsigned long error = 0;
    FILE* input = NULL;
    FILE* output = NULL;
    
    //for sanity, check if output file existed or not
    //and ask for confirmation
    int do_continue=1;
    output = fopen(output_filepath, "rb");
    if (output != NULL)
    {
        fclose(output);
        output = NULL;
        //ask to confirm
        printf("File %s already exists and will be overwritten. Press Y to confirm overwriting the file or any other key to cancel > ",output_filepath);
        if (getc(stdin)!='Y')
        {
            do_continue=0;
        }
        printf("\n");
    }
    
    //return if user cancelled
    if (do_continue==0)
        return error_user_cancel;
    
    //begin read from input file
    input = fopen(input_filepath, "rb");
    
    if (input!=NULL)
    {
        //find file size to allocate space for input string
        size_t file_size = 0;
        fseek(input, 0, SEEK_END);
        file_size = ftell(input);
        rewind(input);
        if (file_size > 0)
        {
            input_string = calloc(file_size,sizeof(char));
            size_t read_pos=0;
            read_pos = fread(input_string, 1, file_size, input);
            if (read_pos != file_size)
            {
                printf("Unable to reach end of file\n");
                error = error_input_read;
            }
            else
            {
                //successfully read everything
                //begin compressing content
                
                output_string = compress_mynl(input_string, file_size, &compress_size, &error);
                
            }
        }
        else
            error = error_empty_input;
    }
    else
        error = error_input_open;
    
    if (compress_size > 0)
    {
        //begin write to output file
        output = fopen(output_filepath, "wb");
        
        if (output != NULL)
        {
            size_t write_pos = 0;
            write_pos = fwrite(output_string, sizeof(char), compress_size, output);
            if (write_pos != compress_size)
                error = error_output_write;
        }
        else
            error = error_output_open;
    }
    else
        error = error_empty_output;
    
    
    //release the krakens
    if (output != NULL)
        fclose(output);
    
    if (input != NULL)
        fclose(input);

    if (input_string != NULL)
        free(input_string);
    
    if (output_string != NULL)
        free(output_string);
    
    return error;
}

//decompress text in file at input_filepath and write to file at output_filepath
unsigned long decompress_mynl_file(const char* input_filepath, const char* output_filepath)
{
    unsigned char* input_string = NULL;
    char* output_string = NULL;
    unsigned long compress_size = 0;
    unsigned long error = 0;
    FILE* input = NULL;
    FILE* output = NULL;
    
    //for sanity, check if output file existed or not
    //and ask for confirmation
    int do_continue=1;
    output = fopen(output_filepath, "rb");
    if (output != NULL)
    {
        fclose(output);
        output = NULL;
        //ask to confirm
        printf("File %s already exists and will be overwritten. Press Y to confirm overwriting the file or any other key to cancel > ",output_filepath);
        if (getc(stdin)!='Y')
        {
            do_continue=0;
        }
        printf("\n");
    }
    
    //return if user cancelled
    if (do_continue==0)
        return error_user_cancel;
    
    //begin read from input file
    input = fopen(input_filepath, "rb");
    
    if (input!=NULL)
    {
        //find file size to allocate space for input string
        size_t file_size = 0;
        fseek(input, 0, SEEK_END);
        file_size = ftell(input);
        rewind(input);
        if (file_size > 0)
        {
            input_string = calloc(file_size,sizeof(char));
            size_t read_pos=0;
            read_pos = fread(input_string, 1, file_size, input);
            if (read_pos != file_size)
            {
                printf("Unable to reach end of file\n");
                error = error_input_read;
            }
            else
            {
                //successfully read everything
                //begin decompressing content
                
                output_string = decompress_mynl(input_string, file_size, &compress_size, &error);
                
            }
        }
        else
            error = error_empty_input;
    }
    else
        error = error_input_open;
    
    if (compress_size > 0)
    {
        //begin write to output file
        output = fopen(output_filepath, "wb");
        
        if (output != NULL)
        {
            size_t write_pos = 0;
            write_pos = fwrite(output_string, sizeof(char), compress_size, output);
            if (write_pos != compress_size)
                error = error_output_write;
        }
        else
            error = error_output_open;
    }
    else
        error = error_empty_output;
    
    
    //release the krakens
    if (output != NULL)
        fclose(output);
    
    if (input != NULL)
        fclose(input);
    
    if (input_string != NULL)
        free(input_string);
    
    if (output_string != NULL)
        free(output_string);
    
    return error;

}

unsigned int get_ngram_code_from_fixed_width_table(const char* input_ngram, int* output_error)
{
    if (keyed_table==NULL)
        *output_error=error_empty_data;
    else
    {
        for (int i=0; i < get_code_table_size(); ++i)
        {
            if (keyed_table[i].type == commonwords)
            {
                for (int j=0; j < mynl_commons_size; ++j)
                {
                    if (strcmp(mynl_commons[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == gram5)
            {
                for (int j=0; j < mynl_5grams_syllable_size; ++j)
                {
                    if (strcmp(mynl_5grams_syllable[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == gram4)
            {
                for (int j=0; j < mynl_4grams_syllable_size; ++j)
                {
                    if (strcmp(mynl_4grams_syllable[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == gram3)
            {
                for (int j=0; j < mynl_3grams_syllable_size; ++j)
                {
                    if (strcmp(mynl_3grams_syllable[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == gram2)
            {
                for (int j=0; j < mynl_2grams_syllable_size; ++j)
                {
                    if (strcmp(mynl_2grams_syllable[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == terminal2)
            {
                for (int j=0; j < mynl_2grams_terminals_size; ++j)
                {
                    if (strcmp(mynl_2grams_terminals[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == terminal1)
            {
                for (int j=0; j < mynl_1grams_terminals_size; ++j)
                {
                    if (strcmp(mynl_1grams_terminals[keyed_table[i].code], input_ngram)==0)
                    {
                        return i;
                    }
                }
            }
            else if (keyed_table[i].type == uncoded && strlen(input_ngram)<2)
            {
                for (int j=0; j < 256; ++j)
                {
                    if (input_ngram[0]==j)
                    {
                        return i;
                    }
                }
            }
        }
    }
    *output_error = error_lookup_fail;
    return 0;
}

void print_consonant_vowel_statistic()
{
    printf("Consonant-vowel syllable patterns\n");
    char pattern[6]="CCCCC";
    int pattern_stat[11]={0};
    pattern_stat[0]=0; //CCVCC
    pattern_stat[1]=0; //CCVC
    pattern_stat[2]=0; //CVCC
    pattern_stat[3]=0; //CVC
    pattern_stat[4]=0; //CVV
    pattern_stat[5]=0; //CV
    pattern_stat[6]=0; //VC
    pattern_stat[7]=0; //VCC
    pattern_stat[8]=0; //CCVV
    
    //2-gram
    for (int i=0; i < mynl_2grams_syllable_size; ++i)
    {
        memset(pattern, 0, 6);
        //check for CC, VV, CV, VC
        for (int j=0; j < 2; ++j)
        {
            if (mynl_2grams_syllable[i][j] != 'a' && mynl_2grams_syllable[i][j] != 'e' && mynl_2grams_syllable[i][j] != 'i' && mynl_2grams_syllable[i][j] != 'o' && mynl_2grams_syllable[i][j] != 'u')
            {
                pattern[j]='C';
            }
            else
            {
                pattern[j]='V';
            }
        }
        
        if (strcmp(pattern,"CC")==0)
        {
            //shouldnt be, something is wrong
            printf("2-gram error: CC\n");
        }
        else if (strcmp(pattern,"CV")==0)
        {
            ++pattern_stat[5];
        }
        else if (strcmp(pattern,"VC")==0)
        {
            ++pattern_stat[6];
        }
        else if (strcmp(pattern,"VV")==0)
        {
            //shouldnt be, something is wrong
            printf("2-gram error: VV\n");
        }
    }
    
    //print stats
    printf("CV : %d / %lu\n", pattern_stat[5], mynl_2grams_syllable_size);
    printf("VC : %d / %lu\n", pattern_stat[6], mynl_2grams_syllable_size);
    
    //3-gram
    for (int i=0; i < mynl_3grams_syllable_size; ++i)
    {
        memset(pattern, 0, 6);
        //check for CVC, CVV, VCC
        for (int j=0; j < 3; ++j)
        {
            if (mynl_3grams_syllable[i][j] != 'a' && mynl_3grams_syllable[i][j] != 'e' && mynl_3grams_syllable[i][j] != 'i' && mynl_3grams_syllable[i][j] != 'o' && mynl_3grams_syllable[i][j] != 'u')
            {
                pattern[j]='C';
            }
            else
            {
                pattern[j]='V';
            }
        }
        
        if (strcmp(pattern,"CCC")==0)
        {
            //shouldnt be, something is wrong
            printf("3-gram error: CCC\n");
        }
        else if (strcmp(pattern,"CVC")==0)
        {
            ++pattern_stat[3];
        }
        else if (strcmp(pattern,"CVV")==0)
        {
            ++pattern_stat[4];
        }
        else if (strcmp(pattern,"VCC")==0)
        {
           ++pattern_stat[7];
        }
        
    }
    
    //print stats
    printf("CVC : %d / %lu\n", pattern_stat[3], mynl_3grams_syllable_size);
    printf("CVV : %d / %lu\n", pattern_stat[4], mynl_3grams_syllable_size);
    printf("VCC : %d / %lu\n", pattern_stat[7], mynl_3grams_syllable_size);
    
    //4-gram
    for (int i=0; i < mynl_4grams_syllable_size; ++i)
    {
        memset(pattern, 0, 6);
        //check for CVC, CVV, VCC
        for (int j=0; j < 4; ++j)
        {
            if (mynl_4grams_syllable[i][j] != 'a' && mynl_4grams_syllable[i][j] != 'e' && mynl_4grams_syllable[i][j] != 'i' && mynl_4grams_syllable[i][j] != 'o' && mynl_4grams_syllable[i][j] != 'u')
            {
                pattern[j]='C';
            }
            else
            {
                pattern[j]='V';
            }
        }
        
        if (strcmp(pattern,"CCVC")==0)
        {
            ++pattern_stat[1];
        }
        else if (strcmp(pattern,"CVCC")==0)
        {
            ++pattern_stat[2];
        }
        else if (strcmp(pattern,"CCVV")==0)
        {
            ++pattern_stat[8];
        }
        
    }
    
    //print stats
    printf("CCVC : %d / %lu\n", pattern_stat[1], mynl_4grams_syllable_size);
    printf("CVCC : %d / %lu\n", pattern_stat[2], mynl_4grams_syllable_size);
    printf("CCVV : %d / %lu\n", pattern_stat[8], mynl_4grams_syllable_size);

    
    
    
    
}
