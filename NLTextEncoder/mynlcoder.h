//
//  mynlcoder.h
//  NLTextEncoder
//
//  Created by yussairi on 12/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef mynlcoder_h
#define mynlcoder_h

#include <stdio.h>

//error enum
enum operation_error { error_input_open = 1, error_input_read = 2, error_output_open = 3, error_output_write = 4, error_empty_output = 5, error_empty_input = 6, error_user_cancel = 7, error_empty_data=8, error_lookup_fail = 9};

void set_verbose();
void toggle_lossy_ignore_capitalization();
void toggle_lossy_reduce_symbol_width();
void toggle_keyed_symbols();
unsigned char* compress_mynl(const char* input_string, unsigned long input_size, unsigned long* compress_size, unsigned long* error);
char* decompress_mynl(const unsigned char* input_string, unsigned long input_size, unsigned long* decompress_size, unsigned long* error);
unsigned long compress_mynl_file(const char* input_filepath, const char* output_filepath);
unsigned long decompress_mynl_file(const char* input_filepath, const char* output_filepath);
unsigned long get_code_table_size();
void create_fixed_width_code_table(unsigned long long key_1, unsigned long long key_2);
void create_variable_width_code_table(unsigned long long key_1, unsigned long long key_2);
void free_code_table();
unsigned int get_ngram_code_from_fixed_width_table(const char* input_ngram, int* output_error);

void print_consonant_vowel_statistic();
#endif /* mynlcoder_h */
