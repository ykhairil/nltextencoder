//
//  mynllexer.c
//  NLTextEncoder
//
//  Created by yussairi on 05/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mynllexer.h"
#include "bitset.h"
#include "mynllexemes.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

const char* current_lexer_version = "1.0.0";
int lexeme_file_line_count = 0;
char** lexeme_file_lines = NULL;
int lexeme_state_count = 0;
unsigned long* lexeme_file_line_lengths = NULL;
char** lexeme_raw=NULL;
int lexeme_raw_count=0;
char** lemma_raw=NULL;
int lemma_raw_count=0;

int transform_stegotext_to_byte(const char* input_data, int input_length, unsigned char* output_text, int* output_length)
{
    
    //read each segment of chars from input
    //the length of the largest segment of
    //current cell in the current state
    int error_flag = 0;
    int current_state=0;
    int current_bit = 0;
    int current_string_end_pos=0;
    
    while (current_state < lexeme_state_count)
    {
        //get the length of the largest cell in
        //current column
        int row0_string_length = 0;
        int row1_string_length = 0;
        char* row0_data = NULL;
        char* row1_data = NULL;
        row0_data = calloc(1024, sizeof(char));
        row1_data = calloc(1024, sizeof(char));
        
        //define candidate substring from input
        char* candidate0_data = NULL;
        char* candidate1_data = NULL;
        
        //get the bit value of rows for current state
        
        
        //get the cell data and also sizes of the strings
        get_state_table_data(current_state, 1, current_bit+1, &row0_data[0], 1024, &row0_string_length);
        get_state_table_data(current_state, 2, current_bit+1, &row1_data[0], 1024, &row1_string_length);
        
        //allocate candidates with the sizes of the cell data
        if (row0_string_length > 0)
            candidate0_data = calloc(row0_string_length, sizeof(char));
        if (row1_string_length > 0)
            candidate1_data = calloc(row1_string_length, sizeof(char));
        
        //get a substring from input data at current search position
        //based on cell data length
        if (row0_string_length > 0)
            memcpy(output_text + current_string_end_pos, candidate0_data, row0_string_length);
        if (row1_string_length > 0)
            memcpy(output_text + current_string_end_pos, candidate1_data, row1_string_length);
        
        //now check if either of the substring match
        //flag for matches
        int is_row0_matched = 0;
        int is_row1_matched = 0;
        if (strcmp(candidate0_data, row0_data)==0)
        {
            is_row0_matched=1;
        }
        if (strcmp(candidate1_data, row1_data)==0)
        {
            is_row1_matched=1;
        }
        
        //release the candidate data
        free(candidate0_data);
        candidate0_data=NULL;
        free(candidate1_data);
        candidate1_data=NULL;
        
        //determine the match
        if (is_row0_matched==1 && is_row1_matched==0)
        {
            //update string search position
            current_string_end_pos+=row0_string_length;
            
            //determine bit value for current row of current state
            //clear off and reallocate the row0_data for this
            free(row0_data);
            row0_data=NULL;
            row0_data = calloc(2, sizeof(char));
            get_state_table_data(current_state, 1, 0, &row0_data[0], 2, &row0_string_length);
            int bitvalue = 0;
            bitvalue = atoi(row0_data);
            if (bitvalue==0)
            {
                set_char_bit(current_bit, &output_text[current_state]);
            }
            else
            {
                clear_char_bit(current_bit, &output_text[current_state]);
            }
            free(row0_data);
            row0_data = NULL;
            
            //update the bit position
            ++current_bit;
        }
        else if (is_row0_matched==0 && is_row1_matched==1)
        {
            //update string search position
            current_string_end_pos+=row1_string_length;
            
            //determine bit value for current row of current state
            //clear off and reallocate the row1_data for this
            free(row1_data);
            row1_data=NULL;
            row1_data = calloc(2, sizeof(char));
            get_state_table_data(current_state, 2, 0, &row1_data[0], 2, &row1_string_length);
            int bitvalue = 0;
            bitvalue = atoi(row1_data);
            if (bitvalue==0)
            {
                set_char_bit(current_bit, &output_text[current_state]);
            }
            else
            {
                clear_char_bit(current_bit, &output_text[current_state]);
            }
            free(row1_data);
            row1_data = NULL;
            
            //update the bit position
            ++current_bit;
        }
        else
        {
            //matching error
            error_flag = 1;
        }
        
        if (row0_data!=NULL)
            free(row0_data);
        if (row1_data!=NULL)
            free(row1_data);
        
        //progress to next state
        //and reset to bit 0
        if (current_bit > 7)
        {
            current_bit = 0;
            ++current_state;
        }
        
        //stop if data exceeded current table's limit
        if (current_state > lexeme_state_count)
            break;
        
        if (error_flag==1)
            break;
    }
    
    return error_flag;
}

void transform_byte_to_stegotext(const char* input_data, int input_length, char* output_text, int* output_length)
{
    //translate each bit in input data
    //into text from the lexeme table
    int current_string_end_pos=0;
    for (int i = 0; i < input_length; ++i)
    {
        //each character is a state
        //so use each lexeme state to
        //process each consequent chars
        
        //get unsigned version of data
        unsigned char data = input_data[i];
        //process each bit in data
        for (int j=0; j < 8; ++j)
        {
            int bitvalue = get_char_bit_value(j, data);
            if (bitvalue==0)
            {
                int string_length = 0;
                char* output_data = NULL;
                output_data = calloc(1024, sizeof(char));
                get_state_table_data(i, 1, j+1, &output_data[0], 1024, &string_length);
                memcpy(output_text + current_string_end_pos, output_data, string_length);
                current_string_end_pos += string_length ;
                *output_length+=string_length;
                free(output_data);
            }
            else
            {
                int string_length = 0;
                char* output_data = NULL;
                output_data = calloc(1024, sizeof(char));
                get_state_table_data(i, 2, j+1, &output_data[0], 1024, &string_length);
                memcpy(output_text + current_string_end_pos, output_data, string_length);
                current_string_end_pos += string_length ;
                *output_length+=string_length;
                free(output_data);
            }
        }
    }
    
}

//get cell data in column column_index and row row_index for state state_index
//into buffer output_cell_data with preallocated size output_length
int get_state_table_data(int state_index, int row_index, int column_index, char* output_cell_data, int output_defined_length, int* output_actual_length)
{
    //structure
    //x0 | c1 | c2 | c3 | c4 | c5 | c6 | c7 | c8
    //r1 | dt | dt |
    //r2 |
    //delimit the line by tab chars
    //strtok() is very problematic so
    //need to tackle this manually
    
    //extrapolate the initial text line index
    //by moving it past the 6-lines header data
    int actual_line_index = (state_index*5) + (7 + row_index);
    
    //sanity check
    if (state_index < 0 || state_index > lexeme_state_count)
        return lexer_invalid_state_index;
    
    if (row_index < 0 || row_index > 2)
        return lexer_invalid_row_index;
    
    if (column_index < 0 && column_index > 8)
        return lexer_invalid_column_index;
    
    //extrapolate a little bit more by a few line indicated by row_data_type
    
    unsigned long current_line_length = lexeme_file_line_lengths[actual_line_index];
    int current_substring_start_pos = 0;
    int current_substring_search_pos = 0;
    
    //establish a counter for column tabs
	int tab_count = 0;
    
    while(current_substring_search_pos < current_line_length)
    {
        char c = lexeme_file_lines[actual_line_index][current_substring_search_pos];
        if (c == '\t' || c == '\n')
        {
			if (tab_count == column_index)
			{
				//reached the required column
                unsigned long data_width = 0;
                data_width = current_substring_search_pos - current_substring_start_pos;
                if (data_width > output_defined_length)
                    data_width = output_defined_length;
                if (data_width > 0)
                    memcpy(output_cell_data, lexeme_file_lines[actual_line_index] + current_substring_start_pos, data_width);
                *output_actual_length = (int)data_width;
                break;
			}
            
            //count column tabs
            ++tab_count;

			//update substring start position
			current_substring_start_pos = current_substring_search_pos + 1;
        }
        ++current_substring_search_pos;
    }
    
    return 0;
}

//generate lexeme transform tables automatically from a given covertext
//the input has to be in series of curly bracket or square bracket-delimited words
//eg. {you}[eat chicken]
//where the given covertext represent the longest form of the stegotext
//the table is generated as a series of bytes
int generate_lexeme_transformation_table_from_covertext_v2(const char* output_file_path, const char* input_covertext, unsigned long input_covertext_length, unsigned int* output_data, int output_length)
{
    //use a lexicon to encode a number of lexemes
    //the index number of these lexemes can then be used as part of a transformation table
    //base text format: {phrase1}[phrase2], where {} refer to a single adjunct transformation cell
    //and [] refer to a single non-adjunct transf cell
    //for an input of {awak}[dah][makan]{sarapan}[ke]{belum}{tu}{?}
    //input in [] is substituted with an alternate form index within the same word class as the original word
    //where the alternative can either be reduced informal form of the original, or a formal synonym
    //while word in {} is substituted with a null char index
    
    int current_substring_search_pos = 0;
    int current_substring_start_pos = 0;
    int cumulated_bits = 0;
    int cumulated_states = 0;
    char* temp_line=NULL;
    int temp_line_end_pos = 0;
    int temp_line_length = 1024;
    int state=0;
    int current_token_end_pos = 0;
    int current_token_start_pos = 0;
    int output_data_counter = 0;
    
    while (current_substring_search_pos < input_covertext_length)
    {
        char c = input_covertext[current_substring_search_pos];
        //first find either { or [
        char substring_to_tokenize[1024]={0};
        
        if (c == '{')
        {
            //start of adjunct tranformable words
            current_token_start_pos = current_substring_search_pos+1;
            state=1;
        }
        else if (c == '}')
        {
            //end of adjunct tranformable words
            //copy token
            current_token_end_pos = current_substring_search_pos-1;
            memcpy(substring_to_tokenize, input_covertext + current_token_start_pos, (current_token_end_pos - current_token_start_pos)+1);
            
            state=0;
        }
        
        if (c == '[')
        {
            //start of nonadjunct tranformable words
            current_token_start_pos = current_substring_search_pos+1;
            state=2;
        }
        else if (c == ']')
        {
            //end of nonadjunct tranformable words
            current_token_end_pos = current_substring_search_pos-1;
            memcpy(substring_to_tokenize, input_covertext + current_token_start_pos, (current_token_end_pos - current_token_start_pos)+1);
            state=0;
        }
        
        //start tokenize all symbols between the brackets
        //cut away when a punctuation is found
        if (strlen(substring_to_tokenize)>0)
        {
            //{abc}
            // ^ ^
            // s e
            // s=start positon, e=end position
            int subtoken_start_position = current_token_start_pos;
            int subtoken_end_position = current_substring_search_pos;
            unsigned long remaining_string_len = strlen(substring_to_tokenize);
            int found=0;
            while (subtoken_start_position < subtoken_end_position)
            {
                for (unsigned int i=0; i < lexeme_raw_count; ++i)
                {
                    char token[20]={0};
                    //get a token the size of the largest token in list and compare
                    int current_token_len = subtoken_end_position - subtoken_start_position;
                    //snprintf(token, current_token_len+1, "%s", input_covertext + subtoken_start_position);
                    memcpy(token, input_covertext + subtoken_start_position, current_token_len);
                    if (strcmp(lexeme_raw[i],token)==0)
                    {
                        output_data[output_data_counter] = i;
                        ++output_data_counter;
                        subtoken_start_position = subtoken_end_position;
                        remaining_string_len -= current_token_len;
                        subtoken_end_position = subtoken_end_position + remaining_string_len;
                        found=1;
                        break;
                    }
                }
                
                if (found==0)
                    --subtoken_end_position;
                if (remaining_string_len == 0)
                    break;
            }
        }
        ++current_substring_search_pos;
    }
    
    return 0;
}

//automatically generate a template of lexeme transformation table file based on
//an input of covertext provided by user. Currently, this file needs to be edited further
//so that each column has unique data. Future revision will include finding synonyms, acronyms
//and adjuncts for automatic replacement
int generate_lexeme_transformation_table_from_covertext(const char* output_file_path, const char* input_covertext, unsigned long input_length, const char* input_issuer, const char* input_desc, const char* input_key)
{
	//structure
	//[0] version
	//[1] issuer name
	//[2] short description
	//[3] key in string form
	//[4] number of states
	//[5] expiry date in string form
	//[6+] state block start
	//[7+] tab-delimited 8 columns description labels, plus 1 column detailing bitset row
	//[8+] tab-delimited 8 columns for first bit value's lexemes,  plus 1 column with either 0 or 1 value
	//[9+] tab-delimited 8 columns for second bit value's lexemes,  plus 1 column with either 0 or 1 value
	//[10+] state block end
	

	//stem the words by each terminal symbol (space, commas)
	//to see how much data can be encoded
	int current_substring_search_pos = 0;
	int current_substring_start_pos = 0;
	int cumulated_bits = 0;
	int cumulated_states = 0;
	char* temp_line=NULL;
	int temp_line_end_pos = 0;
	int temp_line_length = 1024;
	FILE* output = NULL;

	output = fopen(output_file_path, "a");
	if (output == NULL)
		return lexer_error_output_open;

	//template for word class columns
	const char* column_data = "b\t1\t2\t3\t4\t5\t6\t7\t8\n";

	//add the preliminary data first
	fputs(current_lexer_version, output);
	fputs("\n", output);
	fputs(input_issuer, output);
	fputs("\n", output);
	fputs(input_desc, output);
	fputs("\n", output);
	fputs(input_key, output);
	fputs("\n", output);
	//add placeholder for number of states because
	//it's not yet known at this time
	fputs("0", output);
	fputs("\n", output);
    //put in the time one week in the future
    char timebuf[80];
    time_t t = time(NULL);
    struct tm time_str = *localtime(&t);
    sprintf(timebuf, "%d-%d-%d %d:%d:%d\n", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
    fputs(timebuf, output);
	
	while (current_substring_search_pos < input_length)
	{
		char c = input_covertext[current_substring_search_pos];
		//cut away when a punctuation is found
		if (c == ' ' || c == ',' || c == '.' || c == '?' || c == '!' || c == ';')
		{
			//add line terminator and move to next state
			//when bit reached 8
			if (cumulated_bits > 7)
			{
				++cumulated_states;
				//add a CRLF and stop-start block at end of current word
				//and increase buffer cursor position				
				
				temp_line[temp_line_end_pos] = '\n';
				temp_line_end_pos++;
				//write current line twice, the second write server as placeholder
				//for future edits of the file
				fputs(temp_line, output);
				//edit the first char to '1'
				temp_line[0] = '1';
				fputs(temp_line, output);
				//put stop of current state block
				fputs("}\n", output);				
				//clear buffer
				free(temp_line);
				temp_line = NULL;
				temp_line_end_pos = 0;
				cumulated_bits = 0;
			}
			else if (cumulated_bits == 0)
			{
				//put start of next state block
				fputs("{\n", output);
				fputs(column_data, output);
			}
			else
			{
				
			}

			//get substring length
			int substring_length = current_substring_search_pos - current_substring_start_pos;			

			//if this is the start of a new state
			//add a bitset row

			//append to temporary buffer for each line
			if (temp_line == NULL)
			{
				temp_line = calloc(temp_line_length, sizeof(char));
				temp_line[0] = '0';
				temp_line_end_pos++;
				temp_line[1] = '\t';
				temp_line_end_pos++;
			}
			memcpy(temp_line + temp_line_end_pos, input_covertext + current_substring_start_pos, substring_length);


			//for each word processed, add a bit accumulator
			++cumulated_bits;

			//increase buffer cursor position
			temp_line_end_pos += substring_length;

			//add a tab at end of current word
			//and increase buffer cursor position			
			temp_line[temp_line_end_pos] = '\t';
			temp_line_end_pos++;

			//add the terminal letter itself as a word			
			temp_line[temp_line_end_pos] = c;
			temp_line_end_pos++;

			//for each word processed, add a bit accumulator
			++cumulated_bits;
			
			//add line terminator and move to next state
			//when bit reached 8
			if (cumulated_bits > 7)
			{
				++cumulated_states;
				//add a CRLF and stop-start block at end of current word
				//and increase buffer cursor position				
				
				temp_line[temp_line_end_pos] = '\n';
				temp_line_end_pos++;
				//write current line twice, the second write server as placeholder
				//for future edits of the file
				fputs(temp_line, output);
				//edit the first char to '1'
				temp_line[0] = '1';
				fputs(temp_line, output);
				//put stop of current state block
				fputs("}\n", output);				
				//clear buffer
				free(temp_line);
				temp_line = NULL;
				temp_line_end_pos = 0;
				cumulated_bits = 0;
			}			
			else
			{				
				temp_line[temp_line_end_pos] = '\t';
				temp_line_end_pos++;
			}			

			//realloc if line exceeded
			if (temp_line_end_pos > temp_line_length)
			{
				temp_line = realloc(temp_line, temp_line_length * 2);
				temp_line_length *= 2;
			}

			//update substring start position
			current_substring_start_pos = current_substring_search_pos + 1;			
			
		}
		++current_substring_search_pos;
	}

	//add the state blocks	
	if (temp_line!=NULL)
		free(temp_line);
	fclose(output);
	return cumulated_bits;
}

//get the total line of data found in lexeme file
int get_lexeme_file_line_count()
{
    return lexeme_file_line_count;
}

//load the lemmas from wordnet database
int load_wordnet_lemma_table(const char* filepath)
{
    int result=0;
    FILE* input=NULL;
    char readline[1024]={0};
    int linecount = 0;
    int qualitylinecount=0;
    input = fopen(filepath, "rb");
    if (input == NULL)
        return lexer_error_input_open;
    else
    {
        //sense file structure to determine max lines
        unsigned int c = 0;
        do
        {
            c = fgetc(input);
            //check for LF (on Unix) or CR (on OSX). Windows usually use both
            if (c == '\n')
                ++linecount;
        } while (c != EOF);
        
        rewind(input);
        
        //allocate temporary data to store quality lines
        int* quality_line = calloc(linecount, sizeof(int));
        
        //lexeme_raw_count = linecount;
        
        //get each line in file to first find the quality lines
        int line_index = 0;
        while (fgets(readline, 1024, input) != NULL)
        {
            int token_start = 0;
            unsigned int ch = 0;
            int column = -1;
            int rowlength = 0;
            int isstandardmalay=0;
            int isquality=0;
            while (ch < strlen(readline))
            {
                
                char readcol[1024]={0};
                if (readline[ch]=='\t')
                {
                    memcpy(readcol, readline + token_start, rowlength);
                    token_start = ch+1;
                    rowlength=0;
                    ++column;
                }
                
                if (column == 0)
                {
                    //get synset value
                    //get last char of synset to get the word class type
                }
                else if (column == 1)
                {
                    //get language type
                    //for now get only B (bahasamalaysia) or M (standardmalay), ignoring I (bahasaindonesia)
                    if (readcol[0]=='B' || readcol[0]=='M')
                        isstandardmalay=1;
                }
                else if (column == 2)
                {
                    //get quality
                    //get only quality Y or O
                    if (readcol[0]=='Y' || readcol[1]=='O')
                        isquality=1;
                }
                else if (column == 3)
                {
                    //get the actual lemma
                    //then reset column
                    column=0;
                }
                
                ++rowlength;
                ++ch;
            }
            if (isstandardmalay == 1 && isquality == 1)
            {
                //add the lemma length
                quality_line[line_index] = rowlength;
                ++qualitylinecount;
                lemma_raw_count = qualitylinecount;
            }
            ++line_index;
        }
        
        //allocate the data
        //allocate array
        lemma_raw = calloc(qualitylinecount, sizeof(char*));
        
        //then reread to get only quality lemma
        rewind(input);
        line_index=0;
        int quality_line_index = 0;
        while (fgets(readline, 1024, input) != NULL)
        {
            
            if (quality_line[line_index]>0)
            {
                //find last tab
                char* lasttab = strrchr(readline, '\t');
                //get the lemma
                int line_length = quality_line[line_index];
                lemma_raw[quality_line_index] = NULL;
                lemma_raw[quality_line_index] = calloc(line_length+1, sizeof(char));
                memcpy(lemma_raw[quality_line_index], lasttab+1, line_length-2);
                ++quality_line_index;
            }
            
            ++line_index;
        }
        
        free(quality_line);
        fclose(input);
    }
    return result;
    
}

//load the raw lexeme data for coded table generation
int load_lexeme_raw_table(const char* filepath)
{
    int result=0;
    FILE* input=NULL;
    char readline[1024]={0};
    int linecount = 0;
    
    input = fopen(filepath, "rb");
    if (input == NULL)
        return lexer_error_input_open;
    else
    {
        //sense file structure to determine max lines
        unsigned int c = 0;
        do
        {
            c = fgetc(input);
            //check for LF (on Unix) or CR (on OSX). Windows usually use both
            if (c == '\n')
                ++linecount;
        } while (c != EOF);
        
        rewind(input);
        
        //allocate array
        lexeme_raw = calloc(linecount, sizeof(char*));
        lexeme_raw_count = linecount;
        
        //get each line in file
        int line_index = 0;
        while (fgets(readline, 1024, input) != NULL)
        {
            //get null-terminated length of string
            unsigned long line_length = strlen(readline);
            //subtract endings by 2 for both CR and LF
            line_length-=2;
            lexeme_raw[line_index] = NULL;
            lexeme_raw[line_index] = calloc(line_length+1, sizeof(char));
            memcpy(lexeme_raw[line_index], readline, line_length);
            lexeme_raw[line_index][line_length]=0;
            
            //update the line counter
            ++line_index;
        }
        
        fclose(input);
    }
    return result;
}

//release all the loaded data from lexeme raw file
void unload_lexeme_raw_data()
{
    if (lexeme_raw != NULL)
    {
        for (int i=0; i < lexeme_raw_count; ++i)
        {
            free(lexeme_raw[i]);
            lexeme_raw[i]=NULL;
        }
        free(lexeme_raw);
        lexeme_raw = NULL;
        lexeme_raw_count = 0;
    }
}

//release all the loaded data from lexeme raw file
void unload_lemma_raw_data()
{
    if (lemma_raw != NULL)
    {
        for (int i=0; i < lemma_raw_count; ++i)
        {
            free(lemma_raw[i]);
            lemma_raw[i]=NULL;
        }
        free(lemma_raw);
        lemma_raw = NULL;
        lemma_raw_count = 0;
    }
}

//return lemma at index position
char* get_lemma(int index)
{
    if (lemma_raw != NULL)
    {
        return lemma_raw[index];
    }
    else
        return NULL;
}

//get the total line of data found in lemma raw file
int get_lemma_raw_data_count()
{
    return lemma_raw_count;
}


//get the total line of data found in lexeme raw file
int get_lexeme_raw_data_count()
{
    return lexeme_raw_count;
}

//release all the loaded data from lexeme transformation file
void unload_lexeme_transformation_file()
{
    if (lexeme_file_lines != NULL)
    {
        for (int i=0; i < lexeme_file_line_count; ++i)
        {
            free(lexeme_file_lines[i]);
            lexeme_file_lines[i]=NULL;
        }
        free(lexeme_file_lines);
        lexeme_file_lines = NULL;
        free(lexeme_file_line_lengths);
        lexeme_file_line_lengths = NULL;
        lexeme_state_count = 0;
        lexeme_file_line_count = 0;
    }
    
}

//load data in table into memory for future use
//do not forget to release them with unload_lexeme_transformation_file
int load_lexeme_transformation_file(const char* filepath)
{
    int result=0;
    FILE* input=NULL;
    char readline[1024]={0};
    int linecount = 0;
    
    input = fopen(filepath, "rb");
    if (input == NULL)
        return lexer_error_input_open;
    else
    {
        //sense file structure to determine max lines
        int c = 0;
        do
        {
            c = fgetc(input);
            //check for LF (on Unix) or CR (on OSX). Windows usually use both
            if (c == '\n' || c == '\r')
                ++linecount;
        } while (c != EOF);
        
        rewind(input);
        //allocate array
        lexeme_file_lines = calloc(linecount, sizeof(char*));
        lexeme_file_line_lengths = calloc(linecount, sizeof(unsigned long));
        lexeme_file_line_count = linecount;
        
        //get each line in file
        int line_index = 0;
        while (fgets(readline, 1024, input) != NULL)
        {
            //get null-terminated length of string
            unsigned long line_length = strlen(readline);
			lexeme_file_lines[line_index] = NULL;
			lexeme_file_lines[line_index] = calloc(line_length+1, sizeof(char));
			memcpy(lexeme_file_lines[line_index], readline, line_length);
            lexeme_file_lines[line_index][line_length]=0;
            //get the file line's length for future reference
            lexeme_file_line_lengths[line_index]=line_length;
            
            //calculate if it's a state block
            if (lexeme_file_lines[line_index][0]=='{' && lexeme_file_lines[line_index][1]=='\n')
                ++lexeme_state_count;
            
            //update the line counter
            ++line_index;
            
        }
        
        fclose(input);
    }
    return result;
}

//naively check affixes of word by stemming out affixes and producing data based on the affix pattern
int generate_2bit_from_lexeme_affixes(const char* word, unsigned long word_length, int* out_bit1, int* out_bit2)
{
    //00 - root
    //01 - prefix
    //10 - suffix
    //11 - circumfix
    
    //first check if front and back of word has affixes
    int is_root=0;
    int has_prefix=0;
    int has_suffix=0;
    int has_valid_root=0;
    int root_start_pos = 0;
    int root_end_pos = word_length-1;
    char prefix[5]={0};
    char suffix[4]={0};
    char root[16]={0};
    
    //check for root word first
    //to avoid getting false affix eg. 'me' from 'merah' (red)
    for (int i=0; i < mynl_morphemes_size; ++i)
    {
        if (strcmp(word, mynl_morphemes[i])==0)
        {
            is_root=1;
            break;
        }
    }
    
    //check affixation only if word confirmed to be non-morpheme
    if (is_root!=1)
    {
        //get prefix first
        //get first 5 letters or the entire word if it's less than 5
        int prefix_len = 4;
        //find up to 2 letters since shortest prefix is 2
        while (prefix_len > 1)
        {
            memset(prefix, 0, 5);
            //copy letters from front
            if (word_length > prefix_len)
                memcpy(prefix, word, prefix_len);
            else
                memcpy(prefix, word, word_length);
            
            for (int i=0; i < mynl_prefixes_size; ++i)
            {
                if (strcmp(prefix, mynl_prefixes[i])==0)
                {
                    root_start_pos = prefix_len;
                    has_prefix=1;
                    break;
                }
            }
            if (has_prefix==1)
                break;
            --prefix_len;
        }
        
        //get suffix next
        int suffix_len = 3;
        while (suffix_len > 1)
        {
            int prefix_pos=0;
            if (has_prefix==1)
                prefix_pos = root_start_pos;
            memset(suffix, 0, 4);
            //copy letters from back
            if (word_length > suffix_len)
                memcpy(suffix, word + (word_length - suffix_len), suffix_len);
            else
                memcpy(suffix, word, word_length);
            
            for (int i=0; i < mynl_suffixes_size; ++i)
            {
                if (strcmp(suffix, mynl_suffixes[i])==0)
                {
                    root_end_pos = (word_length - suffix_len - 1);
                    has_suffix=1;
                    break;
                }
            }
            if (has_suffix==1)
                break;
            --suffix_len;
        }
        
        //get root to verify affixation
        int root_size = (root_end_pos - root_start_pos)+1;
        memcpy(root,word + root_start_pos, root_size);
        //verify against inflection rule
        if (strcmp(prefix, "ber")==0)
        {
            //root rule : first letter R: remove.
            //root rule : first letter not R : no change
            //check if root is valid
            char candidate[16]={0};
            memcpy(candidate, root, root_size);
            for (int i=0; i < mynl_morphemes_size; ++i)
            {
                if (strcmp(candidate, mynl_morphemes[i])==0)
                {
                    has_valid_root=1;
                    break;
                }
            }
            
            //if no good result retrieved,
            //check if R was removed from root
            //by adding R to candidate root
            if (has_valid_root!=1)
            {
                memset(candidate,0, root_size);
                memcpy(candidate+1, root, root_size);
                candidate[0]='r';
                
                for (int i=0; i < mynl_morphemes_size; ++i)
                {
                    if (strcmp(candidate, mynl_morphemes[i])==0)
                    {
                        has_valid_root=1;
                        break;
                    }
                }
            }
        }
        else if (strcmp(prefix, "mem")==0)
        {
            //root rule : first letter P: change to M.
            //root rule : first letter not P : no change
            //check if root is valid
            char candidate[16]={0};
            memcpy(candidate, root, root_size);
            for (int i=0; i < mynl_morphemes_size; ++i)
            {
                if (strcmp(candidate, mynl_morphemes[i])==0)
                {
                    has_valid_root=1;
                    break;
                }
            }
            
            //if no good result retrieved,
            //check if R was removed from root
            //by adding R to candidate root
            if (has_valid_root!=1)
            {
                memset(candidate,0, root_size);
                memcpy(candidate+1, root, root_size);
                candidate[0]='p';
                
                for (int i=0; i < mynl_morphemes_size; ++i)
                {
                    if (strcmp(candidate, mynl_morphemes[i])==0)
                    {
                        has_valid_root=1;
                        break;
                    }
                }
            }
        }
        else if (strcmp(prefix, "men")==0)
        {
            //root rule : first letter T: change to N.
            //root rule : first letter not T : no change
            //check if root is valid
            char candidate[16]={0};
            memcpy(candidate, root, root_size);
            for (int i=0; i < mynl_morphemes_size; ++i)
            {
                if (strcmp(candidate, mynl_morphemes[i])==0)
                {
                    has_valid_root=1;
                    break;
                }
            }
            
            //if no good result retrieved,
            //check if T was removed from root
            //by adding T to candidate root
            if (has_valid_root!=1)
            {
                memset(candidate,0, root_size);
                memcpy(candidate+1, root, root_size);
                candidate[0]='t';
                
                for (int i=0; i < mynl_morphemes_size; ++i)
                {
                    if (strcmp(candidate, mynl_morphemes[i])==0)
                    {
                        has_valid_root=1;
                        break;
                    }
                }
            }
        }
        else if (strcmp(prefix, "meng")==0)
        {
            //root rule : first letter K: change to NG.
            //root rule : first letter not K : no change
            //check if root is valid
            char candidate[16]={0};
            memcpy(candidate, root, root_size);
            for (int i=0; i < mynl_morphemes_size; ++i)
            {
                if (strcmp(candidate, mynl_morphemes[i])==0)
                {
                    has_valid_root=1;
                    break;
                }
            }
            
            //if no good result retrieved,
            //check if K was removed from root
            //by adding K to candidate root
            if (has_valid_root!=1)
            {
                memset(candidate,0, root_size);
                memcpy(candidate+1, root, root_size);
                candidate[0]='k';
                
                for (int i=0; i < mynl_morphemes_size; ++i)
                {
                    if (strcmp(candidate, mynl_morphemes[i])==0)
                    {
                        has_valid_root=1;
                        break;
                    }
                }
            }
        }
        else if (strcmp(prefix, "meny")==0)
        {
            //root rule : first letter S: change to NY.
            //root rule : first letter not S : no change
            //check if root is valid
            char candidate[16]={0};
            memcpy(candidate, root, root_size);
            for (int i=0; i < mynl_morphemes_size; ++i)
            {
                if (strcmp(candidate, mynl_morphemes[i])==0)
                {
                    has_valid_root=1;
                    break;
                }
            }
            
            //if no good result retrieved,
            //check if S was removed from root
            //by adding S to candidate root
            if (has_valid_root!=1)
            {
                memset(candidate,0, root_size);
                memcpy(candidate+1, root, root_size);
                candidate[0]='s';
                
                for (int i=0; i < mynl_morphemes_size; ++i)
                {
                    if (strcmp(candidate, mynl_morphemes[i])==0)
                    {
                        has_valid_root=1;
                        break;
                    }
                }
            }
        }
        
        
    }
    
    
    
    if (has_prefix==0 && has_suffix==0)
    {
        *out_bit1=0;
        *out_bit2=0;
    }
    else if (has_prefix==1 && has_suffix==0)
    {
        *out_bit1=1;
        *out_bit2=0;
    }
    else if (has_prefix==0 && has_suffix==1)
    {
        *out_bit1=0;
        *out_bit2=1;
    }
    else
    {
        *out_bit1=1;
        *out_bit2=1;
    }
    
    return 0;
}

