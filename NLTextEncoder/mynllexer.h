//
//  mynllexer.h
//  NLTextEncoder
//
//  Created by yussairi on 05/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef mynllexer_h
#define mynllexer_h

#include <stdio.h>

//error enum
enum lexer_operation_error { lexer_error_input_open = 1, lexer_error_input_read = 2, lexer_error_output_open = 3, lexer_error_output_write = 4, lexer_error_empty_output = 5, lexer_error_empty_input = 6, lexer_error_user_cancel = 7};

enum lexer_state_error { lexer_invalid_state_index = 1, lexer_invalid_row_index = 2, lexer_invalid_column_index = 3 };

int load_lexeme_transformation_file(const char* filepath);
int generate_lexeme_transformation_table_from_covertext(const char* output_file_path, const char* input_covertext, unsigned long input_length, const char* output_issuer, const char* output_desc, const char* output_key);
void unload_lexeme_transformation_file();
int get_lexeme_file_line_count();
int get_state_table_data(int state_index, int row_index, int column_index, char* output_cell_data, int output_defined_length, int* output_actual_length);
void transform_byte_to_stegotext(const char* input_data, int input_length, char* output_text, int* output_length);
int transform_stegotext_to_byte(const char* input_data, int input_length, unsigned char* output_text, int* output_length);

int generate_lexeme_transformation_table_from_covertext_v2(const char* output_file_path, const char* input_covertext, unsigned long input_covertext_length, unsigned int* output_data, int output_length);
int load_lexeme_raw_table(const char* filepath);
void unload_lexeme_raw_data();
int get_lexeme_raw_data_count();

int load_wordnet_lemma_table(const char* filepath);
void unload_lemma_raw_data();
int get_lemma_raw_data_count();
char* get_lemma(int index);

//naively check affixes of word by stemming out affixes and producing data based on the affix pattern
int generate_2bit_from_lexeme_affixes(const char* word, unsigned long word_length, int* out_bit1, int* out_bit2);
#endif /* mynllexer_h */
