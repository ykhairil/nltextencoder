//
//  mynllexertests.c
//  NLTextEncoder
//
//  Created by yussairi on 05/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mynllexertests.h"
#include "mynllexer.h"
#include <string.h>
#include <stdlib.h>

#ifdef WIN32
#define test_in_file "E:\\Development\\nltextencoder\\nlt_key_1.tsv"
#elif defined(__APPLE__)
#define test_in_file "/users/syed/documents/nlt_key_1.tsv"
#endif

#ifdef WIN32
#define test_out_file "E:\\Development\\nltextencoder\\nlt_key_0.tsv"
#elif defined(__APPLE__)
#define test_out_file "/users/syed/documents/nlt_key_0.tsv"
#endif

#ifdef WIN32
#define test_raw_lex_file "E:\\Development\\nltextencoder\\nlt_raw_0.tsv"
#elif defined(__APPLE__)
#define test_raw_lex_file "/users/syed/documents/nlt_raw_0.tsv"
#endif

#ifdef WIN32
#define test_raw_wordnet_file "E:\\Development\\nltextencoder\\wn-msa-all.tab"
#elif defined(__APPLE__)
#define test_raw_wordnet_file "/users/admin/documents/wn-msa-all.tab"
#endif

const char* test_covertext = "Saya ni sebenarnya memang suka sangat sangat kat awak ni Alice. Kenapa awak ni macam tak boleh nak terima diri saya ni?!";

void test_transform_data()
{
    //title
    printf("Transformation of 3 byte input data forward and back \n========\n");
    load_lexeme_transformation_file(test_out_file);
    int result = 0;
    int size = 0;
    int data_size = 0;
    char* output = NULL;
    unsigned char* data = NULL;
    output = calloc(1024, sizeof(char));
    data = calloc(5, sizeof(unsigned char));
    transform_byte_to_stegotext("ABC", 3, &output[0], &size);
    if (size != 0)
        printf("[PASS] : 3 byte data is transformed to %d bytes >  %s\n", size, output);
    else
        printf("[FAIL] : Data transformation failure %d\n", result);
    transform_stegotext_to_byte(output, size, &data[0], &data_size);
    
    unload_lexeme_transformation_file();
    free(output);
}


void test_get_state1cell01_from_lexeme_file()
{
    //title
    printf("Retrieval of cell (1,1) from state 1 in lexical transformation table file\n========\n");
    load_lexeme_transformation_file(test_out_file);
    int result = 0;
    int size = 0;
    char* output = NULL;
    output = calloc(1024, sizeof(char));
    get_state_table_data(1, 1, 8, &output[0], 1024, &size);
    if (result == 0)
        printf("[PASS] : A %d byte cell data is extracted without failure, returning %s\n", size, output);
    else
        printf("[FAIL] : Cell data extraction failed with error %d\n", result);
    unload_lexeme_transformation_file();
    free(output);
}

void test_get_state0cell01_from_lexeme_file()
{
    //title
    printf("Retrieval of cell (0,1) from state 0 in lexical transformation table file\n========\n");
    load_lexeme_transformation_file(test_out_file);
    int result = 0;
    int size = 0;
    char* output = NULL;
    output = calloc(1024, sizeof(char));
    get_state_table_data(0, 0, 1, &output[0], 1024, &size);
    if (result == 0)
        printf("[PASS] : A %d byte cell data is extracted without failure, returning %s\n", size, output);
    else
        printf("[FAIL] : Cell data extraction failed with error %d\n", result);
    unload_lexeme_transformation_file();
    free(output);
}

void test_load_lexeme_file()
{
    //title
    printf("Loading of lexical transformation table file\n========\n");
    load_lexeme_transformation_file(test_out_file);
    int size = 0;
    size = get_lexeme_file_line_count();
    if (size > 1)
        printf("[PASS] : Data is extracted from file\n");
    else
        printf("[FAIL] : expected some data loaded, actual result %d\n", size);
    unload_lexeme_transformation_file();
}

void test_create_lexeme_file()
{
	//title
	printf("Lexical transformation table creation\n========\n");
	unsigned long leftover = 0;
	leftover = generate_lexeme_transformation_table_from_covertext(test_out_file, test_covertext, strlen(test_covertext), "NIST", "test", "ABDEFGH12345678");
	if (leftover == 0)
		printf("[PASS] : All covertext words processed");
	else
		printf("[FAIL] : expected 0 codable words left, actual result %lu\n", 8 - leftover);
}

void test_load_lexeme_raw_table()
{
    //title
    printf("Loading of raw lexical file\n========\n");
    load_lexeme_raw_table(test_raw_lex_file);
    int size = 0;
    size = get_lexeme_raw_data_count();
    if (size > 1)
        printf("[PASS] : Data is extracted from file\n");
    else
        printf("[FAIL] : expected some data loaded, actual result %d\n", size);
    unload_lexeme_raw_data();
}

void test_create_lexeme_from_input()
{
    //title
    printf("Creating transformation table from user input\n========\n");
    load_lexeme_raw_table(test_raw_lex_file);
    const char* covertext = "{saya}[suka awak]";
    unsigned int output[100];
    generate_lexeme_transformation_table_from_covertext_v2(test_out_file, covertext, strlen(covertext), &output[0], 100);
    unload_lexeme_raw_data();
}

void test_load_lemma_raw_table()
{
    //title
    printf("Loading of raw lemma file\n========\n");
    load_wordnet_lemma_table(test_raw_wordnet_file);
}

void test_generate_2bits_from_rootword()
{
    //title
    printf("Gettings 2 bits from root word\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("merah", 5, &bit1, &bit2);
    if (bit1 == 0 && bit2 == 0)
        printf("[PASS] : 00 extracted from root word\n");
    else
        printf("[FAIL] : expected 00, actual result %d%d\n", bit1,bit2);
        
}

void test_generate_2bits_from_prefixword()
{
    //title
    printf("Gettings 2 bits from prefixed word\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("merakam", 5, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 0)
        printf("[PASS] : 01 extracted from root word\n");
    else
        printf("[FAIL] : expected 01, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_suffixword()
{
    //title
    printf("Gettings 2 bits from prefixed word\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("rakamkan", 8, &bit1, &bit2);
    if (bit1 == 0 && bit2 == 1)
        printf("[PASS] : 10 extracted from root word\n");
    else
        printf("[FAIL] : expected 10, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_circumfixword()
{
    //title
    printf("Gettings 2 bits from circumfixed word\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("merakamkan", 10, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 1)
        printf("[PASS] : 11 extracted from root word\n");
    else
        printf("[FAIL] : expected 11, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_prefixword_ber()
{
    //title
    printf("Gettings 2 bits from inflected prefixed word 'ber'\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("berunding", 9, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 0)
        printf("[PASS] : 01 extracted from root word\n");
    else
        printf("[FAIL] : expected 01, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_prefixword_mem()
{
    //title
    printf("Gettings 2 bits from inflected prefixed word 'mem'\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("meminjam", 8, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 0)
        printf("[PASS] : 01 extracted from root word\n");
    else
        printf("[FAIL] : expected 01, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_prefixword_men()
{
    //title
    printf("Gettings 2 bits from inflected prefixed word 'men'\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("menukar", 7, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 0)
        printf("[PASS] : 01 extracted from root word\n");
    else
        printf("[FAIL] : expected 01, actual result %d%d\n", bit1,bit2);
    
}

void test_generate_2bits_from_prefixword_meng()
{
    //title
    printf("Gettings 2 bits from inflected prefixed word 'meng'\n========\n");
    int bit1=0;
    int bit2=0;
    generate_2bit_from_lexeme_affixes("mengarut", 8, &bit1, &bit2);
    if (bit1 == 1 && bit2 == 0)
        printf("[PASS] : 01 extracted from root word\n");
    else
        printf("[FAIL] : expected 01, actual result %d%d\n", bit1,bit2);
    
}

void run_all_lexer_tests()
{
    //test_transform_data();
    //test_create_lexeme_file();
    //test_get_state1cell01_from_lexeme_file();
    //test_get_state0cell01_from_lexeme_file();
	
    //test_load_lexeme_file();
    //test_load_lexeme_raw_table();
    //test_load_lemma_raw_table();
    //test_create_lexeme_from_input();
    test_generate_2bits_from_rootword();
    test_generate_2bits_from_prefixword();
    test_generate_2bits_from_suffixword();
    test_generate_2bits_from_circumfixword();
    test_generate_2bits_from_prefixword_ber();
    test_generate_2bits_from_prefixword_mem();
    test_generate_2bits_from_prefixword_men();
    test_generate_2bits_from_prefixword_meng();
}
