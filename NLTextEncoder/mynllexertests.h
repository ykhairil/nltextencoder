//
//  mynllexertests.h
//  NLTextEncoder
//
//  Created by yussairi on 05/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef mynllexertests_h
#define mynllexertests_h

#include <stdio.h>

void run_all_lexer_tests();

#endif /* mynllexertests_h */
