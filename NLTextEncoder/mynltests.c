//
//  mynltests.c
//  NLTextEncoder
//
//  Created by yussairi on 29/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mynltests.h"
#include "mynlcoder.h"
#include "keygen.h"
#include "mynllexer.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef __linux__
# include <sys/sysinfo.h>
#endif

#ifdef __APPLE__
# include <mach/task.h>
# include <mach/mach_init.h>
#include <zlib.h>
#endif

#ifdef WIN32
#define ZLIB_DLL
#include <windows.h>
#include <Psapi.h>
#include <zlib.h>
#else
# include <sys/resource.h>
#endif

//try getting memory in cross-platform ways
void get_memory_usage(int* output_process_memory_amount, int* output_physical_memory_amount)
{
#ifdef WIN32
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
	SIZE_T physMemUsedByMe = pmc.WorkingSetSize;
	*output_process_memory_amount = (int)virtualMemUsedByMe;
	*output_physical_memory_amount = (int)physMemUsedByMe;
#elif defined(__APPLE__)
	// Inspired by:
	// http://miknight.blogspot.com/2005/11/resident-set-size-in-mac-os-x.html
	struct task_basic_info t_info;
	mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
	task_info(current_task(), TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count);
	
    *output_process_memory_amount = (int)t_info.virtual_size;
    *output_physical_memory_amount = (int)t_info.resident_size;
	
#endif


	
}

#pragma mark test cases

void test_short_terminated_malay_word_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Bantu.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Short terminated Malay word\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_long_terminated_malay_word_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Membangunkan.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Long terminated Malay word\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_short_terminated_malay_sentence_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Bantu saya.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Short terminated Malay sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));

    printf("=========\n");
}

void test_long_terminated_malay_sentence_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Membangunkan minda yang bagus.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Long terminated Malay sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));

    printf("=========\n");
}

void test_extralong_terminated_malay_sentence_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Bahawasanya pengiktirafan keutuhan kemuliaan dan hak samarata serta asasi yang tak terpisah bagi seluruh umat manusia adalah asas kebebasan,keadilan dan kedamaian dunia. Bahawasanya pengabaian serta penghinaan terhadap hak asasi manusia telah pun mengakibatkan tindakan terkutuk yang telah melanggari perasaan hati umat manusia, dan munculnya sebuah dunia di mana manusia akan menikmati kebebasan berucap dan menganut kepercayaan serta kebebasan dari rasatakut dan citarasa telah pun diisytiharkan sebagai aspirasi toragung seluruh umat manusia. Sekiranya kita tidak mahu mendorong manusia dalam keadaan terdesak, sebagai pilihan terakhir, memberontak mementang kezaliman serta penindasan, maka adalah penting bagi hak asasi manusia dipertahankan oleh kedaulatan undang-undang. Bahawasanya adalah penting bagi memajukan perkembangan perhubungan persahabatan di antara negara-negara. Bahawasanya rakyat Bangsa-Bangsa Bersatu, dalam Piagamnya telah sekali lagi menegaskan kepercayaan mereka terhadap hak asasi manusia, terhadap kemuliaan serta nilaidiri manusia dan terhadap hak samarata lelaki dan perempuan dan telah menetapkan keazaman untuk memajukan perkembangan sosial dan taraf hidup yang lebih sempurna dalam suasana kebebasan yang lebih luas. Bahawasanya Negara-Negara Anggota telah berikrar untuk masing-masing mencapai, dengan kerjasama Bangsa-Bangsa Bersatu, pengutaraan kehormatan sejagat terhadap, serta pematuhan, hak asasi manusia dan kebebasan asasi. Bahawasanya satu fahaman bersama terhadap hak serta kebebasan ini seluruhnya adalah terpenting demi menjadikan ikrar ini kenyataan sepenuhnya. MAKA DENGAN INI Perhimpunan agung mengisytiharkan Perisytiharan sejagat hak asasi manusia ini sebagai suatu ukuran bersama terhadap pencapaian oleh seluruh umat manusia dan kesemua negara dengan tujuan supaya setiap individu dan setiap badan masyarakat, dengan senantiasa mengingati Perisytiharan ini, hendaklah berazam melalui pengajaran dan pendidikan bagi memajukan sanjungan terhadap seluruh hak-hak dan kebebasan ini dan secara langkah-langkah berperingkat-peringkat, di bidang negara dan antarabangsa, bagi menjaminkan pengkitirafan dan pematuhan sejagatnya yang berkesan, kedua-duanya di antara negara-negara anggota masing-masing dan rakyat wilayah-wilayah di bawah bidangkuasa mereka.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    //set_verbose();
    //title
    printf("Extra Long terminated Malay sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    //set_verbose();
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    
    printf("=========\n");
}


void test_long_terminated_mixed_sentence_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "I suka gi dinner.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Long terminated mixed sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_short_terminated_english_word_lossless()
{
    clock_t t;
    t = clock();
    const char* test_string = "Buffoon!";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

    //title
    printf("Long terminated mixed sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);

	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_long_terminated_malay_common_words_lossless()
{
	clock_t t;
	t = clock();
	const char* test_string = "Orang-orang kerajaan Malaysia.";
	unsigned long original_length = strlen(test_string);
	unsigned long compress_size = 0;
	unsigned long decompress_size = 0;
	unsigned long error = 0;
	unsigned char* compress_result = NULL;
	char* decompress_result = NULL;

	//count memory before operation
	int pre_process_memory = 0;
	int pre_physical_memory = 0;
	get_memory_usage(&pre_process_memory, &pre_physical_memory);

	//title
	printf("Long terminated Malay sentence with common words\n========\n");
	printf("Original: %s, size %ld\n", test_string, original_length);
	compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
	printf("Compress: size %ld\n", compress_size);
	decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
	printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);

	//compression test
	if (compress_size < original_length)
		printf("Compression - [PASS] : %.2f%% compression\n", 100 - ((float)compress_size / (float)original_length) * 100);
	else
		printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
	//decompression test
	if (strcmp(test_string, decompress_result) == 0)
		printf("Decompression - [PASS] : exact input returned\n");
	else
		printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);

	free(compress_result);
	free(decompress_result);
	t = clock() - t;
	double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds

	printf("Duration : %f seconds \n", time_taken);
	//count memory after operation
	int post_process_memory = 0;
	int post_physical_memory = 0;
	get_memory_usage(&post_process_memory, &post_physical_memory);
	printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));

	printf("=========\n");
}

void test_short_terminated_malay_word_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "Bantu.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Short terminated Malay word with lossy cap ignore mode set \n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("bantu.",decompress_result) < 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_long_terminated_malay_word_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "Membangunkan.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Long terminated Malay word with lossy cap ignore mode set\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("membangunkan.",decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_short_terminated_english_word_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "Buffoon!";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Long terminated mixed sentence with lossy cap ignore mode set\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("buffoon!",decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_short_terminated_malay_sentence_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "Bantu saya.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Short terminated Malay sentence with lossy cap ignore mode set\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("bantu saya.",decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_long_terminated_malay_sentence_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "Membangunkan minda yang bagus.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Long terminated Malay sentence with lossy cap ignore mode set\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("membangunkan minda yang bagus.",decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_long_terminated_mixed_sentence_lossy_capitalization_ignore()
{
    clock_t t;
    t = clock();
    toggle_lossy_ignore_capitalization();
    const char* test_string = "I suka gi dinner.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Long terminated mixed sentence\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp("i suka gi dinner.",decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_ignore_capitalization();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_long_terminated_malay_common_words_lossy_capitalization_ignore()
{
	clock_t t;
	t = clock();
	toggle_lossy_ignore_capitalization();
	const char* test_string = "Orang-orang kerajaan Malaysia.";
	unsigned long original_length = strlen(test_string);
	unsigned long compress_size = 0;
	unsigned long decompress_size = 0;
	unsigned long error = 0;
	unsigned char* compress_result = NULL;
	char* decompress_result = NULL;
	//title
	printf("Long terminated Malay sentence with common words and ignoring capitalization \n========\n");
	printf("Original: %s, size %ld\n", test_string, original_length);
	compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
	printf("Compress: size %ld\n", compress_size);
	decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
	printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);

	//compression test
	if (compress_size < original_length)
		printf("Compression - [PASS] : %.2f%% compression\n", 100 - ((float)compress_size / (float)original_length) * 100);
	else
		printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
	//decompression test
	if (strcmp("orang-orang kerajaan malaysia.", decompress_result) == 0)
		printf("Decompression - [PASS] : exact input returned\n");
	else
		printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);

	free(compress_result);
	free(decompress_result);
	toggle_lossy_ignore_capitalization();
	t = clock() - t;
	double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds

	printf("Duration : %f seconds \n", time_taken);
	printf("=========\n");
}

void test_short_terminated_malay_word_lossy_reduce_symbol_width()
{
    clock_t t;
    t = clock();
    toggle_lossy_reduce_symbol_width();
    const char* test_string = "Bantu.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    //title
    printf("Short terminated Malay word\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    toggle_lossy_reduce_symbol_width();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_create_codetable()
{
    clock_t t;
    t = clock();
    //title
    printf("Creating S-box code table with key 5,0\n========\n");
    create_fixed_width_code_table(5, 0);
    free_code_table();
    t = clock() - t;
    double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_get_code_from_codetable()
{
    clock_t t;
    t = clock();
    //title
    printf("Getting code from S-box code table with key 5,0\n========\n");
    create_fixed_width_code_table(5, 0);
    int error=0;
    unsigned int code = 0;
    code = get_ngram_code_from_fixed_width_table("kan", &error);
    free_code_table();
    t = clock() - t;
    double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds
    if (error == 0)
        printf("Retrieval - [PASS] : code is %d for 'kan' \n", code);
    else
        printf("Retrieval - [FAIL] : error is %d for 'kan' \n", error);
    printf("Duration : %f seconds \n", time_taken);
    printf("=========\n");
}

void test_short_terminated_malay_word_keyed()
{
    clock_t t;
    t = clock();
    const char* test_string = "Bantu.";
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    unsigned long decompress_size = 0;
    unsigned long error = 0;
    unsigned char* compress_result = NULL;
    char* decompress_result = NULL;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    toggle_keyed_symbols();
    create_variable_width_code_table(50, 50);
    //title
    printf("Short terminated Malay word with key set to 50,50\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    compress_result = compress_mynl(test_string, strlen(test_string), &compress_size, &error);
    printf("Compress: size %ld\n", compress_size);
    decompress_result = decompress_mynl(compress_result, compress_size, &decompress_size, &error);
    printf("Decompress: %s, size %ld\n", decompress_result, decompress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    //decompression test
    if (strcmp(test_string,decompress_result) == 0)
        printf("Decompression - [PASS] : exact input returned\n");
    else
        printf("Decompression - [FAIL] : expected %s, actual result %s\n", test_string, decompress_result);
    
    free(compress_result);
    free(decompress_result);
    free_code_table();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    toggle_keyed_symbols();
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_short_terminated_malay_word_unix_zlib()
{
    clock_t t;
    t = clock();
    
    const char* test_string = "Bantu.";
    char zipdata[33]={0};
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    
    //title
    printf("Short terminated Malay word compress using zlib\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    
    // zlib struct
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    // setup "a" as the input and "b" as the compressed output
    defstream.avail_in = (uInt)strlen(test_string)+1; // size of input, string + terminator
    defstream.next_in = (Bytef *)test_string; // input char array
    defstream.avail_out = (uInt)32; // size of output
    defstream.next_out = (Bytef *)zipdata; // output char array
    
    // the actual compression work.
    deflateInit(&defstream, Z_BEST_COMPRESSION);
    deflate(&defstream, Z_FINISH);
    deflateEnd(&defstream);
    
    //copy to string and set the compressed size
    compress_size = defstream.total_out;
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_long_terminated_malay_word_unix_zlib()
{
    clock_t t;
    t = clock();
    
    const char* test_string = "Membangunkan.";
    char zipdata[64]={0};
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    
    //title
    printf("Short terminated Malay word compress using zlib\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    
    // zlib struct
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    // setup "a" as the input and "b" as the compressed output
    defstream.avail_in = (uInt)strlen(test_string)+1; // size of input, string + terminator
    defstream.next_in = (Bytef *)test_string; // input char array
    defstream.avail_out = (uInt)63; // size of output
    defstream.next_out = (Bytef *)zipdata; // output char array
    
    // the actual compression work.
    deflateInit(&defstream, Z_BEST_COMPRESSION);
    deflate(&defstream, Z_FINISH);
    deflateEnd(&defstream);
    
    //copy to string and set the compressed size
    compress_size = defstream.total_out;
    printf("Compress : size %lu\n", compress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}


void test_short_terminated_malay_sentence_unix_zlib()
{
    clock_t t;
    t = clock();
    
    const char* test_string = "Bantu saya.";
    char zipdata[64]={0};
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    
    //title
    printf("Long terminated Malay word compress using zlib\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    
    // zlib struct
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    // setup "a" as the input and "b" as the compressed output
    defstream.avail_in = (uInt)strlen(test_string)+1; // size of input, string + terminator
    defstream.next_in = (Bytef *)test_string; // input char array
    defstream.avail_out = (uInt)63; // size of output
    defstream.next_out = (Bytef *)zipdata; // output char array
    
    // the actual compression work.
    deflateInit(&defstream, Z_BEST_COMPRESSION);
    deflate(&defstream, Z_FINISH);
    deflateEnd(&defstream);
    
    //copy to string and set the compressed size
    compress_size = defstream.total_out;
    printf("Compress : size %lu\n", compress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_long_terminated_malay_sentence_unix_zlib()
{
    clock_t t;
    t = clock();
    
    const char* test_string = "Membangunkan minda yang bagus.";
    char zipdata[128]={0};
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    
    //title
    printf("Long terminated Malay word compress using zlib\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    
    // zlib struct
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    // setup "a" as the input and "b" as the compressed output
    defstream.avail_in = (uInt)strlen(test_string)+1; // size of input, string + terminator
    defstream.next_in = (Bytef *)test_string; // input char array
    defstream.avail_out = (uInt)127; // size of output
    defstream.next_out = (Bytef *)zipdata; // output char array
    
    // the actual compression work.
    deflateInit(&defstream, Z_BEST_COMPRESSION);
    deflate(&defstream, Z_FINISH);
    deflateEnd(&defstream);
    
    //copy to string and set the compressed size
    compress_size = defstream.total_out;
    printf("Compress : size %lu\n", compress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}

void test_extralong_terminated_malay_sentence_unix_zlib()
{
    clock_t t;
    t = clock();
    
    const char* test_string = "Bahawasanya pengiktirafan keutuhan kemuliaan dan hak samarata serta asasi yang tak terpisah bagi seluruh umat manusia adalah asas kebebasan,keadilan dan kedamaian dunia. Bahawasanya pengabaian serta penghinaan terhadap hak asasi manusia telah pun mengakibatkan tindakan terkutuk yang telah melanggari perasaan hati umat manusia, dan munculnya sebuah dunia di mana manusia akan menikmati kebebasan berucap dan menganut kepercayaan serta kebebasan dari rasatakut dan citarasa telah pun diisytiharkan sebagai aspirasi toragung seluruh umat manusia. Sekiranya kita tidak mahu mendorong manusia dalam keadaan terdesak, sebagai pilihan terakhir, memberontak mementang kezaliman serta penindasan, maka adalah penting bagi hak asasi manusia dipertahankan oleh kedaulatan undang-undang. Bahawasanya adalah penting bagi memajukan perkembangan perhubungan persahabatan di antara negara-negara. Bahawasanya rakyat Bangsa-Bangsa Bersatu, dalam Piagamnya telah sekali lagi menegaskan kepercayaan mereka terhadap hak asasi manusia, terhadap kemuliaan serta nilaidiri manusia dan terhadap hak samarata lelaki dan perempuan dan telah menetapkan keazaman untuk memajukan perkembangan sosial dan taraf hidup yang lebih sempurna dalam suasana kebebasan yang lebih luas. Bahawasanya Negara-Negara Anggota telah berikrar untuk masing-masing mencapai, dengan kerjasama Bangsa-Bangsa Bersatu, pengutaraan kehormatan sejagat terhadap, serta pematuhan, hak asasi manusia dan kebebasan asasi. Bahawasanya satu fahaman bersama terhadap hak serta kebebasan ini seluruhnya adalah terpenting demi menjadikan ikrar ini kenyataan sepenuhnya. MAKA DENGAN INI Perhimpunan agung mengisytiharkan Perisytiharan sejagat hak asasi manusia ini sebagai suatu ukuran bersama terhadap pencapaian oleh seluruh umat manusia dan kesemua negara dengan tujuan supaya setiap individu dan setiap badan masyarakat, dengan senantiasa mengingati Perisytiharan ini, hendaklah berazam melalui pengajaran dan pendidikan bagi memajukan sanjungan terhadap seluruh hak-hak dan kebebasan ini dan secara langkah-langkah berperingkat-peringkat, di bidang negara dan antarabangsa, bagi menjaminkan pengkitirafan dan pematuhan sejagatnya yang berkesan, kedua-duanya di antara negara-negara anggota masing-masing dan rakyat wilayah-wilayah di bawah bidangkuasa mereka.";
    char zipdata[3000]={0};
    unsigned long original_length = strlen(test_string);
    unsigned long compress_size = 0;
    
    //count memory before operation
    int pre_process_memory = 0;
    int pre_physical_memory = 0;
    get_memory_usage(&pre_process_memory, &pre_physical_memory);
    
    //title
    printf("Extra long terminated Malay word compress using zlib\n========\n");
    printf("Original: %s, size %ld\n", test_string, original_length);
    
    // zlib struct
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    // setup "a" as the input and "b" as the compressed output
    defstream.avail_in = (uInt)strlen(test_string)+1; // size of input, string + terminator
    defstream.next_in = (Bytef *)test_string; // input char array
    defstream.avail_out = (uInt)2999; // size of output
    defstream.next_out = (Bytef *)zipdata; // output char array
    
    // the actual compression work.
    deflateInit(&defstream, Z_BEST_COMPRESSION);
    deflate(&defstream, Z_FINISH);
    deflateEnd(&defstream);
    
    //copy to string and set the compressed size
    compress_size = defstream.total_out;
    printf("Compress : size %lu\n", compress_size);
    
    //compression test
    if (compress_size < original_length)
        printf("Compression - [PASS] : %.2f%% compression\n", 100-((float)compress_size/(float)original_length)*100);
    else
        printf("Compression - [FAIL] : expected less than %lu, actual result %lu\n", strlen(test_string), compress_size);
    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    
    printf("Duration : %f seconds \n", time_taken);
    
    //count memory after operation
    int post_process_memory = 0;
    int post_physical_memory = 0;
    get_memory_usage(&post_process_memory, &post_physical_memory);
    printf("Process / Physical RAM used : %d B / %d B \n", ((post_process_memory - pre_process_memory)), ((post_physical_memory - pre_physical_memory)));
    printf("=========\n");
}


void run_all_mynl_test()
{
    test_get_code_from_codetable();
    test_create_codetable();
    test_short_terminated_malay_word_lossless();
    test_long_terminated_malay_word_lossless();
    test_short_terminated_english_word_lossless();
    test_short_terminated_malay_sentence_lossless();
    test_long_terminated_malay_sentence_lossless();
    test_extralong_terminated_malay_sentence_lossless();
    test_long_terminated_mixed_sentence_lossless();
	test_long_terminated_malay_common_words_lossless();

    test_short_terminated_malay_word_lossy_capitalization_ignore();
    test_long_terminated_malay_word_lossy_capitalization_ignore();
    test_short_terminated_english_word_lossless();
    test_short_terminated_malay_sentence_lossy_capitalization_ignore();
    test_long_terminated_malay_sentence_lossy_capitalization_ignore();
	test_long_terminated_malay_common_words_lossy_capitalization_ignore();
    
    test_short_terminated_malay_word_lossy_reduce_symbol_width();
    test_short_terminated_malay_word_keyed();
    
    test_short_terminated_malay_word_unix_zlib();
    test_long_terminated_malay_word_unix_zlib();
    test_short_terminated_malay_sentence_unix_zlib();
    test_long_terminated_malay_sentence_unix_zlib();
    test_extralong_terminated_malay_sentence_unix_zlib();
}
