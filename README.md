# NLTextEncoder

A C library for compressing natural language text using syllable n-gram stemming and encoding. This encoding algorithm is best suited for compressing short natural language content that would not be compressed efficiently with other frequency-based encoding algorithm such as Lempel-Ziv or DEFLATE (used in zlib and zip). Currently, only support for Malay language encoding is implemented.

## Usage

To use this library in your project, simply add the header codes and the source codes in NLTextEncoder folder to your project. 

The algorithm can be compiled using gcc, msvc, clang or any other C99-compatible C compilers.

## Disclaimer
This work is based off my research in the Journal of Information Science and Applications, which is at its final leg. Therefore some aspects of the algorithm is expected to change from time to time.

## Theory
The algorithm worked by finding repetitive patterns in a natural language text and representing these repetitions as a reduced data. The most obvious repetitions in a natural language is the use of syllables, and these syllables are used as a symbol for the encoding algorithm.

## Methodology
The encoding method is partially based on a variable-width dictionary encoding method. Prior to the development of the algorithm, a list of unique syllables are first built by stemming out syllables from a list of morphemes (root word) of a language, and then reinforced with additional syllables from other features of the language, such as affixes, conjunctions and so on.
The syllables in the list are then checked against the stemming results of words from a list of most frequently used word in corpus texts. The syllables with higher frequency are moved up the list, and vice versa, so that the syllable list is sorted by ascending frequence. 

Afterwards, each syllable in the list is assigned a number. When combined with the previous sorting method, this means that syllable with higher frequency has numbers with shorter binary number width. For example, a hypothetical syllable 'foo' with high frequency value could have decimal 2 (binary 10) index, while another syllable 'bar' with lower frequency value could have decimal 8 (binary 1000). After this step, the list is finally used as a 'dictionary' in the encoding method.

To encode syllables in a word, the input string is first parsed to extract individual words. Each of these individual words are consequently stemmed to extract series of syllables n-grams, using rules from the relevant language. Each n-gram is then looked up in the dictionary. If they are in the dictionary, the n-gram is replaced with its number symbol. If not, each character of the n-gram is encoded individually. The series of binary numbers resulting from these process is then written to a string of bytes. Based on the implementation, these bytes can either be used by other process or written to file.

## Compression performance comparison

Input string | DEFLATE method, default (%) | NLTextEncoder method, default (%)
-------------|-----------------------------|------------
Bantu. | -74.8 | 33.33 
Serang musuh menggunakan pisau Rambo, lepas tu buang mayat ke dalam tasik. | 6.3 | 56.97